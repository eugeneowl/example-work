import { Migration } from '@mikro-orm/migrations';

export class Migration20220512220136 extends Migration {

  async up(): Promise<void> {
    this.addSql('alter table "user" add column "invoice_number_type" text check ("invoice_number_type" in (\'prefix\', \'counter\')) not null default \'counter\', add column "invoice_number_prefix" varchar(10) not null default \'\', add column "invoice_number_counter" int not null default 1;');
    this.addSql('alter table "user" drop constraint if exists "user_email_attachments_check";');
    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{ "invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" drop constraint if exists "user_email_notifications_check";');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{ }\';');
    this.addSql('alter table "user" drop constraint if exists "user_workflow_settings_check";');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{ "autoEmail": false, "autoArchive": false}\';');
    this.addSql('alter table "user" drop column "invoice_number_setting";');

    this.addSql('alter table "client" drop constraint "client_user_id_email_unique";');
    this.addSql('alter table "client" add constraint "client_user_id_email_unique" unique ("user_id", "email");');

    this.addSql('alter table "order" drop constraint if exists "order_order_check";');
    this.addSql('alter table "order" alter column "order" type jsonb using ("order"::jsonb);');
    this.addSql('alter table "order" alter column "order" set default \'{ }\';');
  }

  async down(): Promise<void> {
    this.addSql('alter table "client" drop constraint "client_user_id_email_unique";');
    this.addSql('alter table "client" add constraint "client_user_id_email_unique" unique ("email", "user_id");');

    this.addSql('alter table "order" drop constraint if exists "order_order_check";');
    this.addSql('alter table "order" alter column "order" type jsonb using ("order"::jsonb);');
    this.addSql('alter table "order" alter column "order" set default \'{}\';');

    this.addSql('alter table "user" add column "invoice_number_setting" varchar null default null;');
    this.addSql('alter table "user" drop constraint if exists "user_email_attachments_check";');
    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{"invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" drop constraint if exists "user_email_notifications_check";');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{}\';');
    this.addSql('alter table "user" drop constraint if exists "user_workflow_settings_check";');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{"autoEmail": false, "autoArchive": false}\';');
    this.addSql('alter table "user" drop column "invoice_number_type";');
    this.addSql('alter table "user" drop column "invoice_number_prefix";');
    this.addSql('alter table "user" drop column "invoice_number_counter";');
  }

}
