import { MikroOrmModule } from '@mikro-orm/nestjs';
import { Module } from '@nestjs/common';
import databaseConfig from './mikro-orm.config';

@Module({
	imports: [MikroOrmModule.forRoot(databaseConfig)],
})
export class DatabaseModule {}
