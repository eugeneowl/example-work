import { Migration } from '@mikro-orm/migrations';

export class Migration20220216225115 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table "user" ("id" uuid not null default uuid_generate_v4(), "email" varchar(255) not null, "first_name" varchar(50) not null, "last_name" varchar(50) not null, "company_name" varchar(255) not null, "title" varchar(255) not null, "address_one" varchar(255) not null, "address_two" varchar(255) not null, "city" varchar(255) not null, "country" varchar(100) not null, "zip" varchar(18) not null, "invoice_number_setting" varchar(255) null, "reply_to_email" varchar(255) null, "email_attachments" jsonb not null default \'{ "invoice": false, "receipt": false}\', "email_notifications" jsonb not null default \'{ }\', "workflow_settings" jsonb not null default \'{ "autoEmail": false, "autoArchive": false}\', "avatar_url" varchar(255) null);');
    this.addSql('alter table "user" add constraint "user_email_unique" unique ("email");');
    this.addSql('alter table "user" add constraint "user_pkey" primary key ("id");');

    this.addSql('create table "wallet" ("id" uuid not null default uuid_generate_v4(), "user_id" uuid not null, "public_address" varchar(255) not null, "cryptocurrency" text check ("cryptocurrency" in (\'eth\')) not null, "name" varchar(255) not null);');
    this.addSql('alter table "wallet" add constraint "wallet_public_address_unique" unique ("public_address");');
    this.addSql('alter table "wallet" add constraint "wallet_pkey" primary key ("id");');

    this.addSql('create table "tag" ("id" uuid not null default uuid_generate_v4(), "name" varchar(50) not null, "user_id" uuid null);');
    this.addSql('alter table "tag" add constraint "tag_name_user_id_unique" unique ("name", "user_id");');
    this.addSql('alter table "tag" add constraint "tag_pkey" primary key ("id");');

    this.addSql('create table "refresh_token" ("id" uuid not null default uuid_generate_v4(), "user_id" uuid not null, "is_revoked" boolean not null, "expires" timestamptz(0) not null);');
    this.addSql('alter table "refresh_token" add constraint "refresh_token_pkey" primary key ("id");');

    this.addSql('create table "client" ("id" uuid not null default uuid_generate_v4(), "email" varchar(255) not null, "first_name" varchar(50) not null, "last_name" varchar(50) not null, "company_name" varchar(255) not null, "title" varchar(255) not null, "address_one" varchar(255) not null, "address_two" varchar(255) not null, "city" varchar(255) not null, "country" varchar(100) not null, "zip" varchar(18) not null);');
    this.addSql('alter table "client" add constraint "client_email_unique" unique ("email");');
    this.addSql('alter table "client" add constraint "client_pkey" primary key ("id");');

    this.addSql('create table "invoice" ("id" uuid not null default uuid_generate_v4(), "number" varchar(255) not null, "project_name" varchar(255) not null, "date" date not null, "logo_url" varchar(255) null, "author_id" uuid not null, "invoice_from_email" varchar(255) not null, "invoice_from_first_name" varchar(50) not null, "invoice_from_last_name" varchar(50) not null, "invoice_from_company_name" varchar(255) not null, "invoice_from_title" varchar(255) not null, "invoice_from_address_one" varchar(255) not null, "invoice_from_address_two" varchar(255) not null, "invoice_from_city" varchar(255) not null, "invoice_from_country" varchar(100) not null, "invoice_from_zip" varchar(18) not null, "client_id" uuid not null, "invoice_to_email" varchar(255) not null, "invoice_to_first_name" varchar(50) not null, "invoice_to_last_name" varchar(50) not null, "invoice_to_company_name" varchar(255) not null, "invoice_to_title" varchar(255) not null, "invoice_to_address_one" varchar(255) not null, "invoice_to_address_two" varchar(255) not null, "invoice_to_city" varchar(255) not null, "invoice_to_country" varchar(100) not null, "invoice_to_zip" varchar(18) not null, "due_data" date not null, "auto_reminder" boolean not null, "wallet_name" varchar(255) not null, "wallet_number" varchar(255) not null, "status" text check ("status" in (\'invoice_saved\')) not null, "create_date" timestamptz(0) not null);');
    this.addSql('alter table "invoice" add constraint "invoice_pkey" primary key ("id");');

    this.addSql('create table "service" ("id" uuid not null default uuid_generate_v4(), "invoice_id" uuid not null, "description" varchar(255) not null, "tag_id" uuid not null, "rate" numeric(5, 3) not null, "qty" int not null, "amount" int not null, "total" int not null);');
    this.addSql('alter table "service" add constraint "service_pkey" primary key ("id");');

    this.addSql('alter table "wallet" add constraint "wallet_user_id_foreign" foreign key ("user_id") references "user" ("id") on update cascade;');

    this.addSql('alter table "tag" add constraint "tag_user_id_foreign" foreign key ("user_id") references "user" ("id") on update cascade on delete set null;');

    this.addSql('alter table "refresh_token" add constraint "refresh_token_user_id_foreign" foreign key ("user_id") references "user" ("id") on update cascade;');

    this.addSql('alter table "invoice" add constraint "invoice_author_id_foreign" foreign key ("author_id") references "user" ("id") on update cascade;');
    this.addSql('alter table "invoice" add constraint "invoice_client_id_foreign" foreign key ("client_id") references "client" ("id") on update cascade;');

    this.addSql('alter table "service" add constraint "service_invoice_id_foreign" foreign key ("invoice_id") references "invoice" ("id") on update cascade;');
    this.addSql('alter table "service" add constraint "service_tag_id_foreign" foreign key ("tag_id") references "tag" ("id") on update cascade;');
  }

}
