import { Body, Controller, Get, Param, ParseUUIDPipe, Patch, Post, Query } from '@nestjs/common';
import { ApiBody, ApiTags, ApiParam, ApiQuery, ApiOperation, ApiOkResponse } from '@nestjs/swagger';
import InvoiceStatus from 'libs/database/enums/invoice_status.enum';
import { CREATE, UPDATE } from 'nestjs-joi';
import { UserId } from 'src/common/decorators/user.decorator';
import { ListItemsDto } from 'src/common/dto/list_items.dto';
import OrderBy from 'src/common/types/order_by.enum';
import joi2swagger from 'src/common/utils/joi2swagger';
import { ClientService } from './client.service';
import { ClientDto } from './dto/client.dto';
import ClientListSort from './types/client_list_sort.enum';

@ApiTags('Client')
@Controller('client')
export class ClientController {
	constructor(private clientService: ClientService) {}

	@Post()
	@ApiOperation({ summary: 'Create a new client record' })
	@ApiBody(joi2swagger(ClientDto, CREATE))
	@ApiOkResponse(joi2swagger(ClientDto, 'RESPONSE'))
	async createClient(@UserId() userId: string, @Body() client: ClientDto): Promise<ClientDto> {
		return await this.clientService.createClient(userId, client);
	}

	@Patch(':id')
	@ApiOperation({ summary: 'Update client record' })
	@ApiBody(joi2swagger(ClientDto, UPDATE))
	@ApiParam({ name: 'id', schema: { type: 'string', format: 'uuid' } })
	@ApiOkResponse(joi2swagger(ClientDto, 'RESPONSE'))
	async updateClient(
		@UserId() userId: string,
		@Param('id', ParseUUIDPipe) id: string,
		@Body() client: ClientDto
	): Promise<ClientDto> {
		return await this.clientService.updateClient(userId, id, client);
	}

	@Get()
	@ApiOperation({ summary: 'Get list clients' })
	@ApiQuery({ name: 'page', schema: { type: 'number' }, required: false })
	@ApiQuery({ name: 'sort_by', schema: { enum: Object.values(ClientListSort) }, required: false })
	@ApiQuery({ name: 'order_by', schema: { enum: Object.values(OrderBy) }, required: false })
	@ApiQuery({ name: 'status', schema: { enum: Object.values(InvoiceStatus) }, required: false })
	@ApiOkResponse(joi2swagger(ListItemsDto, 'LIST', ClientDto))
	async listClients(
		@UserId() userId: string,
		@Query('page') page: number | undefined,
		@Query('sort_by') sortBy: ClientListSort | undefined,
		@Query('order_by') orderBy: OrderBy | undefined,
		@Query('status') status: InvoiceStatus | undefined
	): Promise<ListItemsDto<ClientDto>> {
		return await this.clientService.listClients(userId, page, sortBy, orderBy, status);
	}

	@Get(':id')
	@ApiOperation({ summary: 'Get one client by id' })
	@ApiParam({ name: 'id', schema: { type: 'string', format: 'uuid' } })
	@ApiOkResponse(joi2swagger(ClientDto))
	async getClient(@UserId() userId: string, @Param('id', ParseUUIDPipe) id: string): Promise<ClientDto> {
		return await this.clientService.getClient(userId, id);
	}
}
