import Joi from 'joi';
import { CREATE, JoiSchema, UPDATE } from 'nestjs-joi';

export class OrderDto {
	@JoiSchema(Joi.string().uuid().required())
	invoiceId!: string;

	@JoiSchema(Joi.string().required())
	@JoiSchema([UPDATE], Joi.string().forbidden())
	clientCurrency!: string;

	@JoiSchema(Joi.string().email().forbidden())
	@JoiSchema([CREATE], Joi.string().email().optional())
	payersEmail?: string;
}
