import Joi from 'joi';
import { JoiSchema } from 'nestjs-joi';

export class EstimateAmountDto {
	@JoiSchema(Joi.number().required())
	extraFromFee!: number;

	@JoiSchema(Joi.number().required())
	extraToFee!: number;

	@JoiSchema(Joi.number().required())
	from!: string;

	@JoiSchema(Joi.number().required())
	fromAmount!: number;

	@JoiSchema(Joi.number().required())
	fromFee!: number;

	@JoiSchema(Joi.number().required())
	fromRate!: number;

	@JoiSchema(Joi.string().required())
	rateId!: string;

	@JoiSchema(Joi.number().required())
	rateIdExpirationTimestamp!: number;

	@JoiSchema(Joi.string().valid('FLOATING', 'FIXED').required())
	rateType!: 'FLOATING' | 'FIXED';

	@JoiSchema(Joi.string().required())
	to!: string;

	@JoiSchema(Joi.number().required())
	toAmount!: number;

	@JoiSchema(Joi.number().required())
	toFee!: number;

	@JoiSchema(Joi.number().required())
	toRate!: number;
}
