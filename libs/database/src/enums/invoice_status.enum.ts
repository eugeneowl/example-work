enum InvoiceStatus {
	Draft = 'draft',
	New = 'new', // The first phase - saving the invoice in the database,
	PendingPayment = 'pending_payment', // The second phase - after the unlock of the invoice, payment is expected
	Paid = 'paid',
	Archive = 'archive',
}

export default InvoiceStatus;
