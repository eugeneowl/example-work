import Joi from 'joi';
import { getTypeSchema, JoiSchema } from 'nestjs-joi';
import { UserEmailDto } from './user_email.dto';

export class UpdateUserEmailsResponseDto {
	@JoiSchema(Joi.array().items(Joi.string().email()).required())
	duplicate: string[] = [];

	@JoiSchema(
		Joi.array()
			.items(getTypeSchema(UserEmailDto, { group: 'LIST' }))
			.required()
	)
	newEmailList: UserEmailDto[] = [];
}
