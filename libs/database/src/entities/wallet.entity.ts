import { Entity, PrimaryKey, Property, ManyToOne, IdentifiedReference, Unique, Enum } from '@mikro-orm/core';
import Blockchain from '../enums/blockchain.enum';
import UserEntity from './user.entity';

export { Blockchain };

@Entity({ tableName: 'wallet' })
export default class WalletEntity {
	@PrimaryKey({ type: 'uuid', defaultRaw: 'uuid_generate_v4()' })
	id!: string;

	@ManyToOne({ lazy: true, hidden: true })
	user!: IdentifiedReference<UserEntity>;

	@Property()
	@Unique()
	publicAddress!: string;

	@Enum(() => Blockchain)
	blockchain!: Blockchain;
}
