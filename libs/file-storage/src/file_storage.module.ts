import { S3 } from '@aws-sdk/client-s3';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { FileStorageService } from './file_storage.service';

@Module({
	imports: [ConfigModule],
	providers: [
		FileStorageService,
		{
			provide: 'AWS_S3_INSTANCE',
			inject: [ConfigService],
			useFactory: async (configService: ConfigService) => new S3({ region: configService.get('AWS_REGION') }),
		},
	],
	exports: [FileStorageService],
})
export class FileStorageModule {}
