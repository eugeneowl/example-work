enum UserEmailStatus {
	NotVerified = 'not_verified',
	PendingVerification = 'pending_verification',
	Verified = 'verified',
}

export default UserEmailStatus;
