import { Migration } from '@mikro-orm/migrations';

export class Migration20220408192133 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table "order" ("id" uuid not null default uuid_generate_v4(), "status" text check ("status" in (\'WAITING\', \'EXCHANGING\', \'SENDING\', \'COMPLETED\', \'NOT_ENTIRE_WITHDRAW\', \'EXPIRED\', \'CANCELED\', \'FAILED\', \'HOLDED\', \'PREPARED\')) not null, "order" jsonb not null default \'{ }\', "created_at" timestamptz(0) not null, "updated_at" timestamptz(0) not null);');
    this.addSql('alter table "order" add constraint "order_pkey" primary key ("id");');

    this.addSql('alter table "user" drop constraint if exists "user_email_attachments_check";');
    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{ "invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" drop constraint if exists "user_email_notifications_check";');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{ }\';');
    this.addSql('alter table "user" drop constraint if exists "user_workflow_settings_check";');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{ "autoEmail": false, "autoArchive": false}\';');

    this.addSql('alter table "invoice" add column "order_id" uuid null;');
    this.addSql('alter table "invoice" drop constraint if exists "invoice_status_check";');
    this.addSql('alter table "invoice" alter column "status" type text using ("status"::text);');
    this.addSql('alter table "invoice" add constraint "invoice_status_check" check ("status" in (\'draft\', \'saved\', \'invoice\', \'pending_payment\', \'paid\'));');
    this.addSql('alter table "invoice" add constraint "invoice_order_id_foreign" foreign key ("order_id") references "order" ("id") on update cascade on delete set null;');
    this.addSql('alter table "invoice" add constraint "invoice_order_id_unique" unique ("order_id");');
  }

  async down(): Promise<void> {
    this.addSql('alter table "invoice" drop constraint "invoice_order_id_foreign";');

    this.addSql('drop table if exists "order" cascade;');

    this.addSql('alter table "invoice" drop constraint if exists "invoice_status_check";');
    this.addSql('alter table "invoice" alter column "status" type text using ("status"::text);');
    this.addSql('alter table "invoice" add constraint "invoice_status_check" check ("status" in (\'draft\', \'saved\', \'invoice\', \'pending_payment\'));');
    this.addSql('alter table "invoice" drop constraint "invoice_order_id_unique";');
    this.addSql('alter table "invoice" drop column "order_id";');

    this.addSql('alter table "user" drop constraint if exists "user_email_attachments_check";');
    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{"invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" drop constraint if exists "user_email_notifications_check";');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{}\';');
    this.addSql('alter table "user" drop constraint if exists "user_workflow_settings_check";');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{"autoEmail": false, "autoArchive": false}\';');
  }

}
