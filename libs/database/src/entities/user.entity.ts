import { Entity, PrimaryKey, Property, OptionalProps, OneToMany, Collection, Enum } from '@mikro-orm/core';
import InvoiceNumber from '../enums/invoice_number.enum';
import TagEntity from './tag.entity';
import UserEmailEntity from './user_email.entity';
import WalletEntity from './wallet.entity';

export { InvoiceNumber };

@Entity({ tableName: 'user' })
export default class UserEntity {
	[OptionalProps]?: 'emailAttachments' | 'workflowSettings' | 'emailNotifications';

	@PrimaryKey({ type: 'uuid', defaultRaw: 'uuid_generate_v4()' })
	id!: string;

	@Property({ length: 50 })
	firstName = '';

	@Property({ length: 50 })
	lastName = '';

	@Property()
	companyName = '';

	@Property()
	title = '';

	@Property()
	addressOne = '';

	@Property()
	addressTwo = '';

	@Property()
	city = '';

	@Property({ length: 100 })
	country = '';

	@Property({ length: 18 })
	zip = '';

	@Enum(() => InvoiceNumber)
	invoiceNumberType = InvoiceNumber.Counter;

	@Property({ length: 10 })
	invoiceNumberPrefix = '';

	@Property({ hidden: true })
	invoiceNumberCounter = 1;

	@Property({ type: 'json', defaultRaw: `'{ "invoice": false, "receipt": false}'::jsonb` })
	emailAttachments = {
		invoice: false,
		receipt: false,
	};

	@Property({ type: 'json', defaultRaw: `'{ }'::jsonb` })
	emailNotifications = {
		invoicePaid: true,
	};

	@Property({ type: 'json', defaultRaw: `'{ "autoArchive": false}'::jsonb` })
	workflowSettings = {
		autoArchive: false,
	};

	@Property()
	avatarUrl?: string;

	@Property()
	logoUrl?: string;

	@OneToMany({ entity: () => UserEmailEntity, mappedBy: 'user', orphanRemoval: true })
	emails = new Collection<UserEmailEntity>(this);

	@OneToMany({ entity: () => WalletEntity, mappedBy: 'user' })
	wallets = new Collection<WalletEntity>(this);

	@OneToMany({ entity: () => TagEntity, mappedBy: 'user', orphanRemoval: true })
	tags = new Collection<TagEntity>(this);
}
