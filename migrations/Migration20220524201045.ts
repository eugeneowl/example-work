import { Migration } from '@mikro-orm/migrations';

export class Migration20220524201045 extends Migration {

  async up(): Promise<void> {
    this.addSql('alter table "user" drop constraint if exists "user_invoice_number_type_check";');

    this.addSql('alter table "invoice" drop constraint "invoice_client_id_foreign";');

    this.addSql('delete from "invoice" where client_id isnull;');

    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{ "invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{ }\';');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{ "autoEmail": false, "autoArchive": false}\';');
    this.addSql('alter table "user" alter column "invoice_number_type" drop default;');
    this.addSql('alter table "user" alter column "invoice_number_type" type text using ("invoice_number_type"::text);');
    this.addSql('alter table "user" add constraint "user_invoice_number_type_check" check ("invoice_number_type" in (\'prefix\', \'counter\'));');
    this.addSql('alter table "user" alter column "invoice_number_prefix" drop default;');
    this.addSql('alter table "user" alter column "invoice_number_prefix" type varchar(10) using ("invoice_number_prefix"::varchar(10));');
    this.addSql('alter table "user" alter column "invoice_number_counter" drop default;');
    this.addSql('alter table "user" alter column "invoice_number_counter" type int using ("invoice_number_counter"::int);');

    this.addSql('alter table "invoice" alter column "client_id" drop default;');
    this.addSql('alter table "invoice" alter column "client_id" type uuid using ("client_id"::text::uuid);');
    this.addSql('alter table "invoice" alter column "client_id" set not null;');
    this.addSql('alter table "invoice" drop column "invoice_to_email";');
    this.addSql('alter table "invoice" drop column "invoice_to_first_name";');
    this.addSql('alter table "invoice" drop column "invoice_to_last_name";');
    this.addSql('alter table "invoice" drop column "invoice_to_company_name";');
    this.addSql('alter table "invoice" drop column "invoice_to_title";');
    this.addSql('alter table "invoice" drop column "invoice_to_address_one";');
    this.addSql('alter table "invoice" drop column "invoice_to_address_two";');
    this.addSql('alter table "invoice" drop column "invoice_to_city";');
    this.addSql('alter table "invoice" drop column "invoice_to_country";');
    this.addSql('alter table "invoice" drop column "invoice_to_zip";');
    this.addSql('alter table "invoice" add constraint "invoice_client_id_foreign" foreign key ("client_id") references "client" ("id") on update cascade;');

    this.addSql('alter table "order" alter column "order" type jsonb using ("order"::jsonb);');
    this.addSql('alter table "order" alter column "order" set default \'{ }\';');
  }

  async down(): Promise<void> {
    this.addSql('alter table "invoice" drop constraint "invoice_client_id_foreign";');

    this.addSql('alter table "user" drop constraint if exists "user_invoice_number_type_check";');

    this.addSql('alter table "invoice" add column "invoice_to_email" varchar not null default null, add column "invoice_to_first_name" varchar not null default null, add column "invoice_to_last_name" varchar not null default null, add column "invoice_to_company_name" varchar not null default null, add column "invoice_to_title" varchar not null default null, add column "invoice_to_address_one" varchar not null default null, add column "invoice_to_address_two" varchar not null default null, add column "invoice_to_city" varchar not null default null, add column "invoice_to_country" varchar not null default null, add column "invoice_to_zip" varchar not null default null;');
    this.addSql('alter table "invoice" alter column "client_id" drop default;');
    this.addSql('alter table "invoice" alter column "client_id" type uuid using ("client_id"::text::uuid);');
    this.addSql('alter table "invoice" alter column "client_id" drop not null;');
    this.addSql('alter table "invoice" add constraint "invoice_client_id_foreign" foreign key ("client_id") references "client" ("id") on update cascade on delete set null;');

    this.addSql('alter table "order" alter column "order" type jsonb using ("order"::jsonb);');
    this.addSql('alter table "order" alter column "order" set default \'{}\';');

    this.addSql('alter table "user" alter column "invoice_number_type" type text using ("invoice_number_type"::text);');
    this.addSql('alter table "user" add constraint "user_invoice_number_type_check" check ("invoice_number_type" in (\'prefix\', \'counter\'));');
    this.addSql('alter table "user" alter column "invoice_number_type" set default \'counter\';');
    this.addSql('alter table "user" alter column "invoice_number_prefix" type varchar using ("invoice_number_prefix"::varchar);');
    this.addSql('alter table "user" alter column "invoice_number_prefix" set default \'\';');
    this.addSql('alter table "user" alter column "invoice_number_counter" type int4 using ("invoice_number_counter"::int4);');
    this.addSql('alter table "user" alter column "invoice_number_counter" set default 0;');
    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{"invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{}\';');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{"autoEmail": false, "autoArchive": false}\';');
  }

}
