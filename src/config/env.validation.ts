import Joi from 'joi';
import { JoiSchema } from 'nestjs-joi';

enum Environment {
	Development = 'development',
	Production = 'production',
}

export class EnvironmentVariables {
	@JoiSchema(
		Joi.string()
			.valid(...Object.values(Environment))
			.required()
	)
	NODE_ENV!: Environment;

	@JoiSchema(Joi.number().port().required())
	PORT!: number;

	@JoiSchema(Joi.string().min(20).required())
	JWT_SECRET!: string;

	@JoiSchema(Joi.string().hostname().required())
	MIKRO_ORM_HOST!: string;

	@JoiSchema(Joi.string().uri().required())
	API_URL!: string;

	@JoiSchema(Joi.string().uri().required())
	FRONTEND_URL!: string;

	@JoiSchema(Joi.number().port().required())
	MIKRO_ORM_PORT!: number;

	@JoiSchema(Joi.string().min(1).required())
	MIKRO_ORM_USER!: string;

	@JoiSchema(Joi.string().min(1).required())
	MIKRO_ORM_PASSWORD!: string;

	@JoiSchema(Joi.string().min(1).required())
	MIKRO_ORM_DB_NAME!: string;

	@JoiSchema(Joi.string().hostname().required())
	REDIS_HOST!: string;

	@JoiSchema(Joi.number().port().required())
	REDIS_PORT!: string;

	@JoiSchema(Joi.string().required())
	AWS_REGION!: string;

	@JoiSchema(Joi.string().required())
	AWS_ACCESS_KEY_ID!: string;

	@JoiSchema(Joi.string().required())
	AWS_SECRET_ACCESS_KEY!: string;

	@JoiSchema(Joi.string().required())
	AWS_BUCKET_INVOICE!: string;

	@JoiSchema(Joi.string().required())
	AWS_BUCKET_PICTURE!: string;

	@JoiSchema(Joi.string().hostname().required())
	MAIL_HOST!: string;

	@JoiSchema(Joi.number().port().required())
	MAIL_PORT!: string;

	@JoiSchema(Joi.string().required())
	MAIL_FROM!: string;

	@JoiSchema(Joi.string().required())
	ALICEBOB_PRIV_KEY!: string;

	@JoiSchema(Joi.string().required())
	ALICEBOB_PUB_KEY!: string;

	@JoiSchema(Joi.string().required())
	FIREBASE_PROJECT_ID!: string;

	@JoiSchema(Joi.string().required())
	FIREBASE_CLIENT_EMAIL!: string;

	@JoiSchema(Joi.string().required())
	FIREBASE_PRIVATE_KEY!: string;

	@JoiSchema(Joi.string().required())
	GMAIL_CLIENT_ID!: string;

	@JoiSchema(Joi.string().required())
	GMAIL_CLIENT_EMAIL!: string;

	@JoiSchema(Joi.string().required())
	GMAIL_PRIVATE_KEY!: string;
}
