import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { contentParser } from 'fastify-multer';
import { Logger } from 'nestjs-pino';

async function bootstrap() {
	const app = await NestFactory.create<NestFastifyApplication>(AppModule, new FastifyAdapter(), { bufferLogs: true });

	app.useLogger(app.get(Logger));
	app.setGlobalPrefix('api');
	app.enableCors({
		origin: '*',
	});

	const config = new DocumentBuilder()
		.setTitle('Magic Invoices')
		.setVersion('1.0')
		.addBearerAuth({ type: 'http', scheme: 'bearer', bearerFormat: 'JWT' }, 'JWT')
		.addSecurityRequirements('JWT')
		.build();

	const document = SwaggerModule.createDocument(app, config);
	SwaggerModule.setup('docs', app, document);

	const configService = app.get(ConfigService);
	const port = configService.get('PORT');

	await app.register(contentParser);

	await app.listen(port, '0.0.0.0');
}
bootstrap();
