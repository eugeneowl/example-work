import Joi from 'joi';
import { CREATE, JoiSchema, UPDATE } from 'nestjs-joi';

const checkEmail = Joi.string().email().max(255);
const checkName = Joi.string().min(1).max(50);
const checkProperty = Joi.string().max(255);
const checkCountry = Joi.string().allow('').max(100);
const checkZip = Joi.string().allow('').max(18);

export class ClientDto {
	@JoiSchema(Joi.string().uuid().forbidden())
	@JoiSchema(['RESPONSE', 'LIST'], Joi.string().uuid().required())
	id?: string;

	@JoiSchema(checkEmail.required())
	@JoiSchema([UPDATE], checkEmail.forbidden())
	email!: string;

	@JoiSchema(checkName.required())
	@JoiSchema([UPDATE], checkName.optional())
	firstName!: string;

	@JoiSchema(checkName.required())
	@JoiSchema([UPDATE], checkName.optional())
	lastName!: string;

	@JoiSchema(checkProperty.min(1).required())
	@JoiSchema([UPDATE], checkProperty.min(1).optional())
	companyName!: string;

	@JoiSchema(checkProperty.allow('').required())
	@JoiSchema([UPDATE], checkProperty.allow('').optional())
	title!: string;

	@JoiSchema(checkProperty.allow('').required())
	@JoiSchema([UPDATE], checkProperty.allow('').optional())
	addressOne!: string;

	@JoiSchema(checkProperty.allow('').required())
	@JoiSchema([UPDATE], checkProperty.allow('').optional())
	addressTwo!: string;

	@JoiSchema(checkProperty.allow('').required())
	@JoiSchema([UPDATE], checkProperty.allow('').optional())
	city!: string;

	@JoiSchema(checkCountry.required())
	@JoiSchema([UPDATE], checkCountry.optional())
	country!: string;

	@JoiSchema(checkZip.required())
	@JoiSchema([UPDATE], checkZip.optional())
	zip!: string;

	@JoiSchema(Joi.string().uuid().forbidden())
	@JoiSchema(['LIST'], Joi.number().integer().required())
	numberInvoices?: number;

	@JoiSchema(Joi.string().uuid().forbidden())
	@JoiSchema(['LIST'], Joi.number().integer().required())
	totalAmountInvoices?: number;
}
