import { Migration } from '@mikro-orm/migrations';

export class Migration20220524204357 extends Migration {

  async up(): Promise<void> {
    this.addSql('alter table "invoice" drop constraint if exists "invoice_status_check";');

    this.addSql('delete from "invoice" where status in(\'invoice\', \'saved\');');
    this.addSql('delete from "service" where (select 1 from "invoice" where id = invoice_id) isnull;');

    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{ "invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{ }\';');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{ "autoEmail": false, "autoArchive": false}\';');

    this.addSql('alter table "invoice" alter column "status" type text using ("status"::text);');
    this.addSql('alter table "invoice" add constraint "invoice_status_check" check ("status" in (\'draft\', \'new\', \'pending_payment\', \'paid\', \'archive\'));');

    this.addSql('alter table "order" alter column "order" type jsonb using ("order"::jsonb);');
    this.addSql('alter table "order" alter column "order" set default \'{ }\';');
  }

  async down(): Promise<void> {
    this.addSql('alter table "invoice" drop constraint if exists "invoice_status_check";');

    this.addSql('alter table "invoice" alter column "status" type text using ("status"::text);');
    this.addSql('alter table "invoice" add constraint "invoice_status_check" check ("status" in (\'draft\', \'saved\', \'invoice\', \'pending_payment\', \'paid\', \'archive\'));');

    this.addSql('alter table "order" alter column "order" type jsonb using ("order"::jsonb);');
    this.addSql('alter table "order" alter column "order" set default \'{}\';');

    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{"invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{}\';');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{"autoEmail": false, "autoArchive": false}\';');
  }

}
