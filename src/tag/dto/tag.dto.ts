import Joi from 'joi';
import { CREATE, JoiSchema } from 'nestjs-joi';

export class TagDto {
	@JoiSchema(Joi.string().uuid().forbidden())
	@JoiSchema([CREATE], Joi.string().uuid().optional())
	@JoiSchema(['RESPONSE'], Joi.string().uuid().required())
	id?: string;

	@JoiSchema(Joi.string().max(50).required())
	name!: string;
}
