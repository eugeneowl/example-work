import Joi from 'joi';
import { JoiSchema } from 'nestjs-joi';

export class PairsDto {
	@JoiSchema(Joi.string().required())
	from!: string;

	@JoiSchema(Joi.string().required())
	to!: string;

	@JoiSchema(Joi.number().required())
	in!: string;

	@JoiSchema(Joi.number().required())
	out!: string;

	@JoiSchema(Joi.string().valid('FLOATING', 'FIXED').required())
	ratetype!: 'FLOATING' | 'FIXED';

	@JoiSchema(Joi.number().required())
	amount!: string;

	@JoiSchema(Joi.string().required())
	tofee!: string;

	@JoiSchema(Joi.string().required())
	fromfee!: string;

	@JoiSchema(Joi.string().required())
	minamount!: string;

	@JoiSchema(Joi.string().required())
	maxamount!: string;

	@JoiSchema(Joi.string().valid('SELL', 'BUY').required())
	side!: 'SELL' | 'BUY';
}
