import { InjectRepository } from '@mikro-orm/nestjs';
import { EntityRepository } from '@mikro-orm/postgresql';
import { QueryFlag, expr } from '@mikro-orm/core';
import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import TagEntity from 'libs/database/entities/tag.entity';
import { TagDto } from './dto/tag.dto';
import { ListItemsDto } from 'src/common/dto/list_items.dto';
import { UpdateTagsResponseDto } from './dto/update_tags_response.dto';
import UserEntity from 'libs/database/entities/user.entity';
import { SOFT_DELETABLE_FILTER } from 'mikro-orm-soft-delete';

@Injectable()
export class TagService {
	readonly #numberItemsPerPage = 20;

	constructor(
		@InjectRepository(UserEntity)
		private readonly userRepository: EntityRepository<UserEntity>,
		@InjectRepository(TagEntity)
		private readonly tagRepository: EntityRepository<TagEntity>
	) {}

	public async createTag(userId: string, tag: TagDto): Promise<TagDto> {
		const newTag = this.tagRepository.create(tag);

		newTag.user = userId;

		await this.tagRepository.persistAndFlush(newTag);

		return newTag;
	}

	public async updateTag(id: string, userId: string, newTagData: TagDto): Promise<TagDto> {
		const tag = await this.tagRepository.findOne(id);

		if (tag == null) {
			throw new NotFoundException('Tag not found');
		}

		if (tag.user == undefined) {
			throw new NotFoundException("Can't change default tag");
		}

		if (tag.user != userId) {
			throw new NotFoundException("Can't change someone else's tag");
		}

		this.tagRepository.assign(tag, newTagData);

		await this.tagRepository.persistAndFlush(tag);

		return tag;
	}

	public async listTags(userId: string, page = 1): Promise<ListItemsDto<TagDto>> {
		if (page < 1) {
			throw new BadRequestException('Page cannot be less than 1');
		}

		const [tags, count] = await this.tagRepository.findAndCount(
			{ $or: [{ user: null }, { user: userId }] },
			{ offset: (page - 1) * this.#numberItemsPerPage, limit: this.#numberItemsPerPage, flags: [QueryFlag.PAGINATE] }
		);
		const response = ListItemsDto.init(tags, page, count, this.#numberItemsPerPage);

		return response;
	}

	public async updateTagList(id: string, tags: TagDto[]): Promise<UpdateTagsResponseDto> {
		const response = new UpdateTagsResponseDto();
		const user = await this.userRepository.findOne(id, { populate: ['tags'] });

		if (user == null) {
			throw new NotFoundException('User not found');
		}

		const result = tags.reduce(
			(result, tag) => {
				result[Number(tag.id !== undefined)].push(tag.name);

				return result;
			},
			[[], []] as [string[], string[]]
		);

		let [newTags, existTags] = result;

		newTags = [...new Set(newTags)];
		existTags = [...new Set(existTags)];

		const newTagsDictionary = new Map<string, string>();

		for (const tag of newTags) {
			newTagsDictionary.set(tag.toLocaleLowerCase(), tag);
		}

		const duplicateTags = await this.tagRepository.find(
			{ [expr('lower(name)')]: { $in: Array.from(newTagsDictionary.keys()) } },
			{ filters: { [SOFT_DELETABLE_FILTER]: false } }
		);
		const restoredTags: string[] = [];

		for (const duplicateTag of duplicateTags) {
			if (duplicateTag.deletedAt != null) {
				restoredTags.push(duplicateTag.name);
			} else {
				response.duplicate.push(duplicateTag.name);
			}
		}

		for (const tag of restoredTags) {
			newTagsDictionary.delete(tag.toLocaleLowerCase());
		}

		newTags = Array.from(newTagsDictionary.values());

		if (response.duplicate.length > 0) {
			return response;
		}

		if (existTags.length > 0) {
			const removeIds: TagEntity[] = user.tags
				.getItems()
				.filter((tagEntity) => existTags.includes(tagEntity.name) == false);

			user.tags.remove(...removeIds);
		} else {
			user.tags.removeAll();
		}

		if (newTags.length > 0) {
			const insertedValues = newTags.map((name): TagEntity => this.tagRepository.create({ user: id, name }));

			user.tags.add(...insertedValues);
		}

		console.log(restoredTags);

		await this.userRepository.persistAndFlush(user);

		if (restoredTags.length > 0) {
			await this.tagRepository
				.qb()
				.update({ deletedAt: null })
				.where({ name: { $in: restoredTags } })
				.execute();
			//await this.tagRepository.nativeUpdate({ name: { $in: restoredTags } }, { deletedAt: null }, { filters: false });
		}

		response.newTagList = (await user.tags.init()).getItems();

		return response;
	}
}
