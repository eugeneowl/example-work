import Joi from 'joi';
import { CREATE, getTypeSchema, JoiSchema, UPDATE } from 'nestjs-joi';
import { Collection } from '@mikro-orm/core';
import UserEmailEntity from 'libs/database/entities/user_email.entity';
import { UserEmailDto } from './user_email.dto';
import { WalletDto } from './wallet.dto';
import WalletEntity from 'libs/database/entities/wallet.entity';
import InvoiceNumber from 'libs/database/enums/invoice_number.enum';

const checkName = Joi.string().min(1).max(50);
const checkProperty = Joi.string().min(1).max(255);
const checkCountry = Joi.string().min(1).max(100);
const checkZip = Joi.string().min(1).max(18);
const checkInvoiceNumber = Joi.string()
	.min(0)
	.max(10)
	.regex(/^[^ ]*$/, 'without spaces');
const checkEmailAttachments = Joi.object({
	invoice: Joi.boolean().optional(),
	receipt: Joi.boolean().optional(),
});
const checkEmailNotifications = Joi.object({
	invoicePaid: Joi.boolean().optional(),
});
const checkWorkflowSettings = Joi.object({
	autoArchive: Joi.boolean().optional(),
});

export class UserDto {
	@JoiSchema(Joi.string().uuid().forbidden())
	@JoiSchema(['LIST'], Joi.string().uuid().required())
	id!: string;

	@JoiSchema(
		Joi.array()
			.items(getTypeSchema(UserEmailDto, { group: 'LIST' }))
			.required()
	)
	@JoiSchema([CREATE], Joi.array().items(getTypeSchema(UserEmailDto)).min(1).required())
	@JoiSchema([UPDATE], Joi.array().forbidden())
	emails!: Collection<UserEmailEntity>;

	@JoiSchema(Joi.array().items(getTypeSchema(WalletDto)).required())
	@JoiSchema([CREATE, UPDATE], Joi.array().forbidden())
	wallets!: Collection<WalletEntity>;

	@JoiSchema(checkName.required())
	@JoiSchema([UPDATE], checkName.optional())
	firstName!: string;

	@JoiSchema(checkName.required())
	@JoiSchema([UPDATE], checkName.optional())
	lastName!: string;

	@JoiSchema(checkProperty.required())
	@JoiSchema([UPDATE], checkProperty.optional())
	companyName!: string;

	@JoiSchema(checkProperty.required())
	@JoiSchema([UPDATE], checkProperty.optional())
	title!: string;

	@JoiSchema(checkProperty.required())
	@JoiSchema([UPDATE], checkProperty.optional())
	addressOne!: string;

	@JoiSchema(checkProperty.required())
	@JoiSchema([UPDATE], checkProperty.optional())
	addressTwo!: string;

	@JoiSchema(checkProperty.required())
	@JoiSchema([UPDATE], checkProperty.optional())
	city!: string;

	@JoiSchema(checkCountry.required())
	@JoiSchema([UPDATE], checkCountry.optional())
	country!: string;

	@JoiSchema(checkZip.required())
	@JoiSchema([UPDATE], checkZip.optional())
	zip!: string;

	@JoiSchema(
		Joi.string()
			.valid(...Object.values(InvoiceNumber))
			.required()
	)
	@JoiSchema(
		[UPDATE],
		Joi.string()
			.valid(...Object.values(InvoiceNumber))
			.optional()
	)
	invoiceNumberType!: InvoiceNumber;

	@JoiSchema(checkInvoiceNumber.optional())
	invoiceNumberPrefix?: string;

	@JoiSchema(checkEmailAttachments.optional())
	emailAttachments?: {
		invoice?: boolean;
		receipt?: boolean;
	};

	@JoiSchema(checkEmailNotifications.optional())
	emailNotifications?: {
		invoicePaid?: boolean;
	};

	@JoiSchema(checkWorkflowSettings.optional())
	workflowSettings?: {
		autoArchive?: boolean;
	};

	@JoiSchema(checkProperty.optional())
	@JoiSchema([CREATE, UPDATE], checkProperty.forbidden())
	avatarUrl?: string;
}
