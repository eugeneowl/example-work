import { BadRequestException, Injectable } from '@nestjs/common';

@Injectable()
export class PictureService {
	static imageFileFilter = (req: Request, file: Express.Multer.File, callback: any) => {
		if (['image/png', 'image/jpg', 'image/jpeg'].includes(file.mimetype) == false) {
			return callback(new BadRequestException('Only .png, .jpg and .jpeg format allowed'));
		}

		callback(null, true);
	};
}
