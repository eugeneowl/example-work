import { Options, LoadStrategy } from '@mikro-orm/core';
import { TsMorphMetadataProvider } from '@mikro-orm/reflection';

const databaseConfig = {
	metadataProvider: TsMorphMetadataProvider,
	type: 'postgresql',
	entities: ['./dist/libs/database/src/entities'],
	entitiesTs: ['./libs/database/src/entities'],
	path: 'dist/migrations',
	pathTs: 'migrations',
	loadStrategy: LoadStrategy.JOINED,
	forceUtcTimezone: true,
	seeder: {
		path: './dist/src/seeders',
		pathTs: '.src/seeders',
	},
	migrations: {
		path: './dist/migrations',
		pathTs: './migrations',
		snapshot: false,
	},
	debug: true,
} as Options;

export default databaseConfig;
