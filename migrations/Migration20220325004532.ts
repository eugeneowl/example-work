import { Migration } from '@mikro-orm/migrations';

export class Migration20220325004532 extends Migration {

  async up(): Promise<void> {
    this.addSql('alter table "user" drop constraint if exists "user_email_attachments_check";');
    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{ "invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" drop constraint if exists "user_email_notifications_check";');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{ }\';');
    this.addSql('alter table "user" drop constraint if exists "user_workflow_settings_check";');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{ "autoEmail": false, "autoArchive": false}\';');

    this.addSql('alter table "client" add column "avatar_url" varchar(255) null;');
  }

  async down(): Promise<void> {
    this.addSql('alter table "client" drop column "avatar_url";');

    this.addSql('alter table "user" drop constraint if exists "user_email_attachments_check";');
    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{"invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" drop constraint if exists "user_email_notifications_check";');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{}\';');
    this.addSql('alter table "user" drop constraint if exists "user_workflow_settings_check";');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{"autoEmail": false, "autoArchive": false}\';');
  }

}
