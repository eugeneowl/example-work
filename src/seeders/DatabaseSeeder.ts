import type { EntityManager } from '@mikro-orm/core';
import { Seeder } from '@mikro-orm/seeder';
import tags from 'src/config/default_tag.json';
import TagEntity from 'libs/database/entities/tag.entity';

export class DatabaseSeeder extends Seeder {
	async run(em: EntityManager): Promise<void> {
		const countTags = await em.count(TagEntity);

		if (countTags == 0) {
			for (const tag of tags) {
				em.create(TagEntity, { name: tag });
			}
		}
	}
}
