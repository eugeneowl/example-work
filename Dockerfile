FROM node:18.2.0-alpine3.15 As production

WORKDIR /usr/src/app

COPY . .

RUN apk add --no-cache \
    chromium \
    nss \
    freetype \
    harfbuzz \
    ca-certificates \
    ttf-freefont

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true \
    PUPPETEER_EXECUTABLE_PATH=/usr/bin/chromium-browser

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

# install dependencies
RUN npm ci --ignore-scripts --omit=dev

# build application
RUN npm run build

EXPOSE 3000

CMD ["./entrypoint.sh"]
