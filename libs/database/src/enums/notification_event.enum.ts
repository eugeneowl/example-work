enum NotificationEvent {
	InvoiceSent = 'invoice_sent',
	InvoiceChangeStatus = 'invoice_change_status',
	EmailVerification = 'email_verification',
	InvoicePaid = 'invoice_paid',
}

export default NotificationEvent;
