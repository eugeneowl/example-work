import { Migration } from '@mikro-orm/migrations';

export class Migration20220413202944 extends Migration {

  async up(): Promise<void> {
    this.addSql('alter table "wallet" drop constraint if exists "wallet_cryptocurrency_check";');
    this.addSql('update "wallet" set cryptocurrency = \'ETH\';');
    this.addSql('alter table "wallet" alter column "cryptocurrency" type text using ("cryptocurrency"::text);');
    this.addSql('alter table "wallet" add constraint "wallet_cryptocurrency_check" check ("cryptocurrency" in (\'ETH\'));');
  }

  async down(): Promise<void> {
    this.addSql('alter table "wallet" drop constraint if exists "wallet_cryptocurrency_check";');
    this.addSql('update "wallet" set cryptocurrency = \'eth\';');
    this.addSql('alter table "wallet" alter column "cryptocurrency" type text using ("cryptocurrency"::text);');
    this.addSql('alter table "wallet" add constraint "wallet_cryptocurrency_check" check ("cryptocurrency" in (\'eth\'));');
  }

}
