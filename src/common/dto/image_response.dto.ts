import Joi from 'joi';
import { JoiSchema } from 'nestjs-joi';

export class ImageResponseDto {
	@JoiSchema(Joi.string().required())
	image!: string;
}
