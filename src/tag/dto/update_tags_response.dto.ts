import Joi from 'joi';
import { getTypeSchema, JoiSchema } from 'nestjs-joi';
import { TagDto } from './tag.dto';

export class UpdateTagsResponseDto {
	@JoiSchema(Joi.array().items(Joi.string().max(50)).required())
	duplicate: string[] = [];

	@JoiSchema(
		Joi.array()
			.items(getTypeSchema(TagDto, { group: 'RESPONSE' }))
			.required()
	)
	newTagList: TagDto[] = [];
}
