import { Body, Controller, Get, Param, ParseUUIDPipe, Patch, Post, Headers, UploadedFile } from '@nestjs/common';
import { ApiBody, ApiTags, ApiOperation, ApiOkResponse, ApiParam, ApiConsumes, ApiHeader } from '@nestjs/swagger';
import { CREATE, UPDATE } from 'nestjs-joi';
import joi2swagger, { joi2swaggerList } from 'src/common/utils/joi2swagger';
import { UserService } from './user.service';
import { UserDto } from './dto/user.dto';
import { UserId } from 'src/common/decorators/user.decorator';
import { UpdateUserEmailsResponseDto } from './dto/update_user_emails_response.dto';
import { FastifyFileInterceptor } from 'nest-fastify-multer';
import { memoryStorage } from 'multer';
import { PictureService } from 'src/picture/picture.service';
import { ImageResponseDto } from '../common/dto/image_response.dto';
import { UserEmailDto } from './dto/user_email.dto';

@ApiTags('User')
@Controller('user')
export class UserController {
	constructor(private userService: UserService) {}

	@Patch()
	@ApiOperation({ summary: 'Update current user record' })
	@ApiBody(joi2swagger(UserDto, UPDATE))
	@ApiOkResponse({ status: 200 })
	async updateClient(@UserId() id: string, @Body() updateUserDto: UserDto): Promise<void> {
		await this.userService.updateUser(id, updateUserDto);
	}

	@Get()
	@ApiOperation({ summary: 'Get current user' })
	@ApiOkResponse(joi2swagger(UserDto))
	async getClient(@UserId() id: string): Promise<UserDto> {
		return await this.userService.getUser(id);
	}

	@Post('emails')
	@ApiOperation({ summary: 'Update email list' })
	@ApiBody(joi2swaggerList(UserEmailDto, CREATE))
	@ApiOkResponse(joi2swagger(UpdateUserEmailsResponseDto))
	async updateEmail(@UserId() id: string, @Body() emails: UserEmailDto[]): Promise<UpdateUserEmailsResponseDto> {
		return await this.userService.updateEmailList(id, emails);
	}

	@Patch('verify-email/:emailId')
	@ApiOperation({ summary: 'Verify email' })
	@ApiHeader({
		name: 'origin',
		required: false,
	})
	@ApiParam({ name: 'emailId', schema: { type: 'string', format: 'uuid' } })
	@ApiOkResponse({ status: 200 })
	async verifyEmail(
		@UserId() id: string,
		@Param('emailId', ParseUUIDPipe) emailId: string,
		@Headers('origin') origin?: string
	): Promise<void> {
		await this.userService.verifyEmail(id, emailId, origin);
	}

	@Patch('confirm-email/:token')
	@ApiOperation({ summary: 'Confirm email' })
	@ApiParam({ name: 'token', schema: { type: 'string', format: 'uuid' } })
	@ApiOkResponse({ status: 200 })
	async confirmEmail(@UserId() id: string, @Param('token', ParseUUIDPipe) token: string): Promise<void> {
		await this.userService.confirmEmail(id, token);
	}

	@Post('avatar')
	@ApiOperation({ summary: 'Set avatar' })
	@FastifyFileInterceptor('avatar', {
		storage: memoryStorage(),
		fileFilter: PictureService.imageFileFilter,
		limits: {
			fileSize: 1048576,
		},
	})
	@ApiOkResponse(joi2swagger(ImageResponseDto))
	@ApiConsumes('multipart/form-data')
	@ApiBody({
		schema: {
			type: 'object',
			required: ['avatar'],
			properties: {
				avatar: {
					type: 'string',
					format: 'binary',
				},
			},
		},
	})
	async updateAvatar(@UserId() id: string, @UploadedFile() file: Express.Multer.File): Promise<ImageResponseDto> {
		return await this.userService.saveImage(id, file, 'avatar');
	}

	@Post('logo')
	@ApiOperation({ summary: 'Set logo' })
	@FastifyFileInterceptor('logo', {
		storage: memoryStorage(),
		fileFilter: PictureService.imageFileFilter,
		limits: {
			fileSize: 1048576,
		},
	})
	@ApiOkResponse(joi2swagger(ImageResponseDto))
	@ApiConsumes('multipart/form-data')
	@ApiBody({
		schema: {
			type: 'object',
			required: ['logo'],
			properties: {
				logo: {
					type: 'string',
					format: 'binary',
				},
			},
		},
	})
	async updateLogo(@UserId() id: string, @UploadedFile() file: Express.Multer.File): Promise<ImageResponseDto> {
		return await this.userService.saveImage(id, file, 'logo');
	}
}
