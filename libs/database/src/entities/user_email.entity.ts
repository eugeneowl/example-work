import { Entity, PrimaryKey, Property, Unique, ManyToOne, OptionalProps, Enum } from '@mikro-orm/core';
import UserEmailStatus from '../enums/user_email_status.enum';
import UserEntity from './user.entity';

export { UserEmailStatus };

@Entity({ tableName: 'user_email' })
export default class UserEmailEntity {
	[OptionalProps]?: 'isDefault' | 'status';

	@PrimaryKey({ type: 'uuid', defaultRaw: 'uuid_generate_v4()' })
	id!: string;

	@Property()
	@Unique()
	email = '';

	@Property()
	isDefault = false;

	@Enum(() => UserEmailStatus)
	status = UserEmailStatus.NotVerified;

	@ManyToOne(() => UserEntity, { mapToPk: true, hidden: true })
	user!: string;
}
