import { BadRequestException, Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import crypto from 'crypto';
import axios from 'axios';
import { ConfigService } from '@nestjs/config';
import InvoiceEntity, { InvoiceStatus } from 'libs/database/entities/invoice.entity';
import { OrderType } from './types/order.type';
import { InjectRepository } from '@mikro-orm/nestjs';
import { EntityData } from '@mikro-orm/core';
import { EntityRepository } from '@mikro-orm/postgresql';
import { OrderEntity, OrderStatus } from 'libs/database/entities/order.entity';
import { all, create } from 'mathjs';
import WalletEntity from 'libs/database/entities/wallet.entity';
import { PairsDto } from './dto/pairs.dto';
import { EstimateAmountDto } from './dto/estimate_amount.dto';
import { ResponseCurrencyToDto } from './dto/response_currency_to.dto copy';
import { OrderAliceBobDto } from './dto/order_alice_bob.dto';
import { NotificationService } from 'src/notification/notification.service';
import ClientEntity from 'libs/database/entities/client.entity';

@Injectable()
export class PaymentService {
	readonly #aliceBobApi = 'https://exchange.alice-bob.io/api/v3/';
	readonly #userId = 'magic.invoices';
	readonly #webhookHandlerUrl;
	readonly #alicebobPrivKey;
	readonly #alicebobPubKey;
	readonly #math;

	constructor(
		@InjectRepository(InvoiceEntity)
		private readonly invoiceRepository: EntityRepository<InvoiceEntity>,
		@InjectRepository(WalletEntity)
		private readonly walletRepository: EntityRepository<WalletEntity>,
		@InjectRepository(OrderEntity)
		private readonly orderRepository: EntityRepository<OrderEntity>,
		@InjectRepository(ClientEntity)
		private readonly clientRepository: EntityRepository<ClientEntity>,
		private readonly notificationService: NotificationService,
		configService: ConfigService
	) {
		this.#alicebobPrivKey = configService.get('ALICEBOB_PRIV_KEY') ?? '';
		this.#alicebobPubKey = configService.get('ALICEBOB_PUB_KEY') ?? '';

		const apiUrl = configService.get('API_URL') ?? '';

		this.#webhookHandlerUrl = `${apiUrl}/payment/webhook-handler`;

		const config = {
			epsilon: 1e-30,
		};

		this.#math = create(all, config);
	}

	async createOrder(id: string, currencyFrom: string, email = '', userId?: string): Promise<OrderAliceBobDto> {
		const invoice = await this.invoiceRepository.findOne(id);

		if (invoice == null) {
			throw new NotFoundException('Invoice not found');
		}

		if (invoice.status != InvoiceStatus.PendingPayment) {
			throw new NotFoundException('Invoice already paid');
		}

		const client = await this.clientRepository.findOne(invoice.client);

		if (client == null) {
			throw new NotFoundException('Client not found');
		}

		const wallet = await this.walletRepository.findOne({ publicAddress: invoice.walletNumber });

		if (wallet == null) {
			throw new NotFoundException('The wallet specified in the invoice is not available');
		}

		currencyFrom = currencyFrom.toUpperCase();
		const currencyTo = wallet.blockchain;

		const pairInfo = await this.sendRequest(`get-pair-info/${currencyFrom}/${currencyTo}`, 'get');
		const minAmount = this.#math.bignumber(pairInfo.minamount);
		const maxAmount = this.#math.bignumber(pairInfo.maxamount);
		const cents = 100;
		const dollars = this.#math.chain(invoice.total).divide(cents).round(2).done();
		const exchangeRate = await this.getExchangeRate(currencyFrom);
		const amount = this.#math.divide(dollars, exchangeRate).toString();

		if (this.#math.smaller(amount, minAmount)) {
			throw new BadRequestException('Amount less than minimum value');
		}

		if (this.#math.larger(amount, maxAmount)) {
			throw new BadRequestException('The amount is greater than the maximum value');
		}

		const payersEmail = email == '' ? client.email : email;
		const orderEntity = this.orderRepository.create({ invoice, payersEmail });

		await this.orderRepository.persistAndFlush(orderEntity);

		let order = undefined;

		try {
			order = await this.sendRequest('create-order', 'post', {
				from: currencyFrom,
				fromAmount: amount,
				to: currencyTo,
				toPaymentDetails: wallet.publicAddress,
				userId: this.#userId,
				partnerOrderId: orderEntity.id,
			});
		} catch (error) {
			await this.orderRepository.removeAndFlush(orderEntity);

			throw error;
		}

		orderEntity.order = order;

		await this.orderRepository.persistAndFlush(orderEntity);

		if (userId !== undefined) {
			invoice.clientUser = userId;
			this.invoiceRepository.persistAndFlush(invoice);
		}

		return order;
	}

	async cancelOrder(id: string): Promise<void> {
		const order = await this.orderRepository.findOne(id);

		if (order == null) {
			throw new NotFoundException('Order not found');
		}

		await this.sendRequest('cancel-order', 'post', {
			id: order.order.id,
		});
	}

	async getPairsInfo(): Promise<PairsDto[]> {
		return await this.sendRequest('get-pairs-info', 'get');
	}

	async estimateAmount(id: string, currencyFrom: string): Promise<EstimateAmountDto> {
		const invoice = await this.invoiceRepository.findOne(id);

		if (invoice == null) {
			throw new NotFoundException('Invoice not found');
		}

		const wallet = await this.walletRepository.findOne({ publicAddress: invoice.walletNumber });

		if (wallet == null) {
			throw new NotFoundException('The wallet specified in the invoice is not available');
		}

		currencyFrom = currencyFrom.toUpperCase();
		const currencyTo = wallet.blockchain;

		const cents = 100;
		const dollars = this.#math.chain(invoice.total).divide(cents).round(2).done();
		const exchangeRate = await this.getExchangeRate(currencyFrom);
		const amount = this.#math.divide(dollars, exchangeRate).toString();

		return await this.sendRequest('estimate-amount', 'post', {
			from: currencyFrom,
			fromAmount: amount,
			to: currencyTo,
		});
	}

	async getCurrencyTo(id: string): Promise<ResponseCurrencyToDto> {
		const invoice = await this.invoiceRepository.findOne(id);

		if (invoice == null) {
			throw new NotFoundException('Invoice not found');
		}

		const wallet = await this.walletRepository.findOne({ publicAddress: invoice.walletNumber });

		if (wallet == null) {
			throw new NotFoundException('The wallet specified in the invoice is not available');
		}

		return {
			to: wallet.blockchain,
		};
	}

	async registerWebhook() {
		await this.sendRequest('webhook/create', 'post', { url: this.#webhookHandlerUrl });
	}

	async webhookHandler(order: OrderType) {
		if (order.partnerOrderId == null) {
			return;
		}

		const orderEntity = await this.orderRepository.findOne(order.partnerOrderId, { populate: ['invoice', 'invoice.author'] });

		if (orderEntity == null) {
			return;
		}

		const updateData: EntityData<OrderEntity> = { status: order.status, order: order };

		orderEntity.status = order.status;
		orderEntity.order = order;

		if (orderEntity.status == OrderStatus.Completed) {
			const workflowSettings = orderEntity.invoice.getProperty('author').getProperty('workflowSettings');

			updateData.invoice = {
				status: workflowSettings.autoArchive ? InvoiceStatus.Archive : InvoiceStatus.Paid,
			};
		}

		this.orderRepository.assign(orderEntity, updateData);

		await this.orderRepository.persistAndFlush(orderEntity);

		if (orderEntity.status == OrderStatus.Completed) {
			const invoice = orderEntity.invoice.getEntity();
			this.notificationService.notifyInvoicePaid(invoice.author.id, invoice.id, invoice.invoiceFromEmail);
		}
	}

	private async sendRequest(endpoint: string, method: 'get' | 'post', body: { [key: string]: string | number } = {}) {
		const now = +new Date();
		const keys = Object.keys(body).sort();

		let initString = '';

		for (const key of keys) {
			initString += key.toLowerCase() + body[key].toString().toLowerCase();
		}

		initString += 'timestamp' + now;

		const signature = crypto.createHmac('SHA512', this.#alicebobPrivKey).update(initString).digest('hex');

		try {
			const response = await axios(`${this.#aliceBobApi}${endpoint}`, {
				method: method,
				headers: {
					'accept': 'application/json',
					'content-type': 'application/json',
					'public-key': this.#alicebobPubKey,
					'timestamp': now.toString(),
					'signature': signature,
				},
				data: JSON.stringify(body),
			});

			return response.data;
		} catch (error: any) {
			const response = error.response;

			console.log('AliceBob Error');
			console.log(response.data);

			if (response.status == 400) {
				switch (response.data.errorCode) {
					case 'INVALID_PAIR':
						throw new BadRequestException('There is no payment option for the specified currency');
						break;
				}
			}

			throw new InternalServerErrorException();
		}
	}

	private async getExchangeRate(currency: string) {
		try {
			const response = await axios(`https://api.binance.com/api/v3/ticker/price?symbol=${currency}USDT`, {
				method: 'GET',
				headers: {
					accept: 'application/json',
				},
			});

			return response.data.price;
		} catch (error: any) {
			const response = error.response;

			console.log('Binance Error');
			console.log(response.data);

			throw new InternalServerErrorException();
		}
	}
}
