import { Migration } from '@mikro-orm/migrations';

export class Migration20220531184258 extends Migration {

  async up(): Promise<void> {
    this.addSql('alter table "user_email" drop constraint if exists "user_email_status_check";');

    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{ "invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{ }\';');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{ "autoEmail": false, "autoArchive": false}\';');

    this.addSql('alter table "user_email" alter column "status" drop default;');
    this.addSql('alter table "user_email" alter column "status" type text using ("status"::text);');
    this.addSql('alter table "user_email" add constraint "user_email_status_check" check ("status" in (\'not_verified\', \'pending_verification\', \'verified\'));');

    this.addSql('alter table "tag" add column "deleted_at" timestamptz(0) null;');

    this.addSql('alter table "order" alter column "order" type jsonb using ("order"::jsonb);');
    this.addSql('alter table "order" alter column "order" set default \'{ }\';');
  }

  async down(): Promise<void> {
    this.addSql('alter table "user_email" drop constraint if exists "user_email_status_check";');

    this.addSql('alter table "order" alter column "order" type jsonb using ("order"::jsonb);');
    this.addSql('alter table "order" alter column "order" set default \'{}\';');

    this.addSql('alter table "tag" drop column "deleted_at";');

    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{"invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{}\';');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{"autoEmail": false, "autoArchive": false}\';');

    this.addSql('alter table "user_email" alter column "status" type text using ("status"::text);');
    this.addSql('alter table "user_email" add constraint "user_email_status_check" check ("status" in (\'not_verified\', \'pending_verification\', \'verified\'));');
    this.addSql('alter table "user_email" alter column "status" set default \'not_verified\';');
  }

}
