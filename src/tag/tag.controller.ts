import { Body, Controller, Get, Param, ParseUUIDPipe, Patch, Post, Query } from '@nestjs/common';
import { ApiBody, ApiTags, ApiParam, ApiQuery, ApiOperation, ApiOkResponse } from '@nestjs/swagger';
import { CREATE, UPDATE } from 'nestjs-joi';
import { ListItemsDto } from 'src/common/dto/list_items.dto';
import joi2swagger, { joi2swaggerList } from 'src/common/utils/joi2swagger';
import { TagService } from './tag.service';
import { TagDto } from './dto/tag.dto';
import { UserId } from 'src/common/decorators/user.decorator';
import { UpdateTagsResponseDto } from './dto/update_tags_response.dto';

@ApiTags('Tag')
@Controller('tag')
export class TagController {
	constructor(private tagService: TagService) {}

	@Post()
	@ApiOperation({ summary: 'Create a new tag record' })
	@ApiBody(joi2swagger(TagDto))
	@ApiOkResponse(joi2swagger(TagDto, 'RESPONSE'))
	async createTag(@UserId() userId: string, @Body() tag: TagDto): Promise<TagDto> {
		return await this.tagService.createTag(userId, tag);
	}

	@Patch(':id')
	@ApiOperation({ summary: 'Update tag record' })
	@ApiBody(joi2swagger(TagDto, UPDATE))
	@ApiParam({ name: 'id', schema: { type: 'string', format: 'uuid' } })
	@ApiOkResponse(joi2swagger(TagDto, 'RESPONSE'))
	async updateTag(@Param('id', ParseUUIDPipe) id: string, @UserId() userId: string, @Body() tag: TagDto): Promise<TagDto> {
		return await this.tagService.updateTag(id, userId, tag);
	}

	@Get()
	@ApiOperation({ summary: 'Get list tags' })
	@ApiQuery({ name: 'page', schema: { type: 'number' }, required: false })
	@ApiOkResponse(joi2swagger(ListItemsDto, 'RESPONSE', TagDto))
	async listTags(@UserId() userId: string, @Query('page') page: number | undefined): Promise<ListItemsDto<TagDto>> {
		return await this.tagService.listTags(userId, page);
	}

	@Post('list')
	@ApiOperation({ summary: 'Update tag list' })
	@ApiBody(joi2swaggerList(TagDto, CREATE))
	@ApiOkResponse(joi2swagger(UpdateTagsResponseDto))
	async updateTagList(@UserId() id: string, @Body() tags: TagDto[]): Promise<UpdateTagsResponseDto> {
		return await this.tagService.updateTagList(id, tags);
	}
}
