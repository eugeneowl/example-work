import { Migration } from '@mikro-orm/migrations';

export class Migration20220426190253 extends Migration {

  async up(): Promise<void> {
    this.addSql('alter table "user" drop constraint if exists "user_email_attachments_check";');
    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{ "invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" drop constraint if exists "user_email_notifications_check";');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{ }\';');
    this.addSql('alter table "user" drop constraint if exists "user_workflow_settings_check";');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{ "autoEmail": false, "autoArchive": false}\';');

    this.addSql('alter table "client" drop constraint "client_email_unique";');
    this.addSql('alter table "client" add constraint "client_user_id_email_unique" unique ("user_id", "email");');

    this.addSql('alter table "invoice" drop constraint if exists "invoice_status_check";');
    this.addSql('alter table "invoice" alter column "status" type text using ("status"::text);');
    this.addSql('alter table "invoice" add constraint "invoice_status_check" check ("status" in (\'draft\', \'saved\', \'invoice\', \'pending_payment\', \'paid\', \'archive\'));');

    this.addSql('alter table "order" drop constraint if exists "order_order_check";');
    this.addSql('alter table "order" alter column "order" type jsonb using ("order"::jsonb);');
    this.addSql('alter table "order" alter column "order" set default \'{ }\';');
  }

  async down(): Promise<void> {
    this.addSql('alter table "client" drop constraint "client_user_id_email_unique";');
    this.addSql('alter table "client" add constraint "client_email_unique" unique ("email");');

    this.addSql('alter table "invoice" drop constraint if exists "invoice_status_check";');
    this.addSql('alter table "invoice" alter column "status" type text using ("status"::text);');
    this.addSql('alter table "invoice" add constraint "invoice_status_check" check ("status" in (\'draft\', \'saved\', \'invoice\', \'pending_payment\', \'paid\'));');

    this.addSql('alter table "order" drop constraint if exists "order_order_check";');
    this.addSql('alter table "order" alter column "order" type jsonb using ("order"::jsonb);');
    this.addSql('alter table "order" alter column "order" set default \'{}\';');

    this.addSql('alter table "user" drop constraint if exists "user_email_attachments_check";');
    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{"invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" drop constraint if exists "user_email_notifications_check";');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{}\';');
    this.addSql('alter table "user" drop constraint if exists "user_workflow_settings_check";');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{"autoEmail": false, "autoArchive": false}\';');
  }

}
