import { Entity, PrimaryKey, Property, Enum, ManyToOne, OptionalProps, IdentifiedReference } from '@mikro-orm/core';
import OrderStatus from '../enums/order_status.enum';
import InvoiceEntity from './invoice.entity';

export { OrderStatus };

@Entity({ tableName: 'order' })
export class OrderEntity {
	[OptionalProps]?: 'status' | 'createdAt' | 'updatedAt' | 'order';

	@PrimaryKey({ type: 'uuid', defaultRaw: 'uuid_generate_v4()' })
	id!: string;

	@ManyToOne({ hidden: true })
	invoice!: IdentifiedReference<InvoiceEntity>;

	@Enum(() => OrderStatus)
	status: OrderStatus = OrderStatus.Waiting;

	@Property()
	payersEmail!: string;

	@Property({ type: 'json', defaultRaw: `'{ }'::jsonb`, hidden: true })
	order: any = {};

	@Property()
	createdAt = new Date();

	@Property({ onUpdate: () => new Date() })
	updatedAt = new Date();
}
