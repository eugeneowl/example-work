import OrderStatus from 'libs/database/enums/order_status.enum';

export class OrderType {
	extraFromFee!: number;
	extraToFee!: number;
	from!: string;
	fromAmount!: number;
	fromAmountReceived!: number;
	fromFee!: number;
	fromPaymentDetails!: string;
	fromRate!: number;
	fromTxHash!: string;
	id!: string;
	partnerOrderId!: string | null;
	payCryproMemo!: string;
	payCryptoAddress!: string;
	payUrl!: string;
	rateType!: 'FLOATING' | 'FIXED';
	redirectUrl!: string;
	status!: OrderStatus;
	to!: string;
	toAmount!: number;
	toFee!: number;
	toMemo!: string;
	toPaymentDetails!: string;
	toRate!: number;
	toTxHash!: string;
	userId!: string;
}
