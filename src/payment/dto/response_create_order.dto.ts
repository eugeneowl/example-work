import Joi from 'joi';
import { JoiSchema } from 'nestjs-joi';

export class ResponseCreateOrderDto {
	@JoiSchema(Joi.string().required())
	cryptoAddress!: string;
}
