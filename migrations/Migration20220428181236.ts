import { Migration } from '@mikro-orm/migrations';

export class Migration20220428181236 extends Migration {

  async up(): Promise<void> {
    this.addSql('alter table "user" drop constraint if exists "user_email_attachments_check";');
    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{ "invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" drop constraint if exists "user_email_notifications_check";');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{ }\';');
    this.addSql('alter table "user" drop constraint if exists "user_workflow_settings_check";');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{ "autoEmail": false, "autoArchive": false}\';');

    this.addSql('alter table "wallet" drop column "name";');
    this.addSql('alter table "wallet" rename column "cryptocurrency" to "blockchain";');

    this.addSql('alter table "notification" drop constraint if exists "notification_key_check";');
    this.addSql('alter table "notification" alter column "key" type text using ("key"::text);');
    this.addSql('alter table "notification" add constraint "notification_key_check" check ("key" in (\'invoice_sent\', \'invoice_change_status\', \'email_verification\', \'invoice_paid\'));');

    this.addSql('alter table "client" drop constraint "client_user_id_email_unique";');
    this.addSql('alter table "client" add constraint "client_user_id_email_unique" unique ("user_id", "email");');

    this.addSql('alter table "order" drop constraint if exists "order_order_check";');
    this.addSql('alter table "order" alter column "order" type jsonb using ("order"::jsonb);');
    this.addSql('alter table "order" alter column "order" set default \'{ }\';');
  }

  async down(): Promise<void> {
    this.addSql('alter table "client" drop constraint "client_user_id_email_unique";');
    this.addSql('alter table "client" add constraint "client_user_id_email_unique" unique ("email", "user_id");');

    this.addSql('alter table "notification" drop constraint if exists "notification_key_check";');
    this.addSql('alter table "notification" alter column "key" type text using ("key"::text);');
    this.addSql('alter table "notification" add constraint "notification_key_check" check ("key" in (\'invoice_sent\', \'invoice_change_status\', \'email_verification\'));');

    this.addSql('alter table "order" drop constraint if exists "order_order_check";');
    this.addSql('alter table "order" alter column "order" type jsonb using ("order"::jsonb);');
    this.addSql('alter table "order" alter column "order" set default \'{}\';');

    this.addSql('alter table "user" drop constraint if exists "user_email_attachments_check";');
    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{"invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" drop constraint if exists "user_email_notifications_check";');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{}\';');
    this.addSql('alter table "user" drop constraint if exists "user_workflow_settings_check";');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{"autoEmail": false, "autoArchive": false}\';');

    this.addSql('alter table "wallet" add column "name" varchar not null default null;');
    this.addSql('alter table "wallet" rename column "blockchain" to "cryptocurrency";');
  }

}
