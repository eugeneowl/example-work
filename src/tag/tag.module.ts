import { Module } from '@nestjs/common';
import { TagService } from './tag.service';
import { TagController } from './tag.controller';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import TagEntity from 'libs/database/entities/tag.entity';
import UserEntity from 'libs/database/entities/user.entity';

@Module({
	imports: [MikroOrmModule.forFeature([TagEntity, UserEntity])],
	providers: [TagService],
	controllers: [TagController],
})
export class TagModule {}
