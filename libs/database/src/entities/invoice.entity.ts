import {
	Entity,
	PrimaryKey,
	Property,
	DateType,
	IdentifiedReference,
	ManyToOne,
	OneToMany,
	Enum,
	OptionalProps,
	Collection,
} from '@mikro-orm/core';
import InvoiceStatus from '../enums/invoice_status.enum';
import ClientEntity from './client.entity';
import { OrderEntity } from './order.entity';
import ServiceEntity from './service.entity';
import UserEntity from './user.entity';

export { InvoiceStatus };

@Entity({ tableName: 'invoice' })
export default class InvoiceEntity {
	[OptionalProps]?: 'createDate' | 'status';

	@PrimaryKey({ type: 'uuid', defaultRaw: 'uuid_generate_v4()' })
	id!: string;

	@Property()
	number!: string;

	@Property()
	projectName!: string;

	@Property()
	logoUrl?: string;

	@ManyToOne(() => UserEntity, { lazy: true, hidden: true })
	author!: IdentifiedReference<UserEntity>;

	@Property()
	invoiceFromEmail!: string;

	@Property({ length: 50 })
	invoiceFromFirstName!: string;

	@Property({ length: 50 })
	invoiceFromLastName!: string;

	@Property()
	invoiceFromCompanyName!: string;

	@Property()
	invoiceFromTitle!: string;

	@Property()
	invoiceFromAddressOne!: string;

	@Property()
	invoiceFromAddressTwo!: string;

	@Property()
	invoiceFromCity!: string;

	@Property({ length: 100 })
	invoiceFromCountry!: string;

	@Property({ length: 18 })
	invoiceFromZip!: string;

	@ManyToOne(() => UserEntity, { hidden: true, mapToPk: true })
	clientUser?: string;

	@ManyToOne()
	client!: IdentifiedReference<ClientEntity>;

	@Property({ type: DateType })
	dueDate!: Date;

	@Property()
	autoReminder = false;

	@Property()
	walletNumber!: string;

	@Enum(() => InvoiceStatus)
	status = InvoiceStatus.New;

	@Property()
	createDate = new Date();

	@OneToMany({ entity: () => ServiceEntity, mappedBy: 'invoice' })
	services = new Collection<ServiceEntity>(this);

	@Property()
	total!: number;

	@OneToMany({ entity: () => OrderEntity, mappedBy: 'invoice' })
	orders = new Collection<OrderEntity>(this);
}
