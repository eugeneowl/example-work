import Joi from 'joi';
import { JoiSchema } from 'nestjs-joi';
import Blockchain from 'libs/database/enums/blockchain.enum';

export class WalletDto {
	@JoiSchema(Joi.string().uuid().required())
	id!: string;

	@JoiSchema(Joi.string().required())
	publicAddress!: string;

	@JoiSchema(
		Joi.string()
			.valid(...Object.values(Blockchain))
			.required()
	)
	blockchain!: Blockchain;
}
