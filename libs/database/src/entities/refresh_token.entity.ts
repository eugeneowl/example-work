import { Entity, PrimaryKey, Property, ManyToOne, IdentifiedReference, OptionalProps } from '@mikro-orm/core';
import UserEntity from './user.entity';

@Entity({ tableName: 'refresh_token' })
export default class RefreshTokenEntity {
	[OptionalProps]?: 'isRevoked';

	@PrimaryKey({ type: 'uuid', defaultRaw: 'uuid_generate_v4()' })
	id!: string;

	@ManyToOne()
	user!: IdentifiedReference<UserEntity>;

	@Property()
	isRevoked = false;

	@Property()
	expires!: Date;
}
