import { Body, Controller, Get, Param, ParseUUIDPipe, Patch, Post } from '@nestjs/common';
import { ApiBody, ApiOkResponse, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';
import { CREATE } from 'nestjs-joi';
import { Public } from 'src/common/decorators/public.decorator';
import { UserId } from 'src/common/decorators/user.decorator';
import joi2swagger, { joi2swaggerList } from 'src/common/utils/joi2swagger';
import { EstimateAmountDto } from './dto/estimate_amount.dto';
import { OrderDto } from './dto/order.dto';
import { OrderAliceBobDto } from './dto/order_alice_bob.dto';
import { PairsDto } from './dto/pairs.dto';
import { ResponseCurrencyToDto } from './dto/response_currency_to.dto copy';
import { PaymentService } from './payment.service';
import { OrderType } from './types/order.type';

@ApiTags('Payment')
@Controller('payment')
export class PaymentController {
	constructor(private paymentService: PaymentService) {}

	@Post('create-order')
	@ApiOperation({ summary: 'Create order' })
	@ApiBody(joi2swagger(OrderDto, CREATE))
	@ApiOkResponse(joi2swagger(OrderAliceBobDto))
	async createOrder(@Body() createOrderDto: OrderDto, @UserId() userId?: string): Promise<OrderAliceBobDto> {
		return await this.paymentService.createOrder(createOrderDto.invoiceId, createOrderDto.clientCurrency, userId);
	}

	@Patch('cancel-order/:id')
	@ApiOperation({ summary: 'Cancel order' })
	@ApiParam({ name: 'id', schema: { type: 'string', format: 'uuid' } })
	@ApiOkResponse({ status: 200 })
	async cancelOrder(@Param('id', ParseUUIDPipe) id: string): Promise<void> {
		await this.paymentService.cancelOrder(id);
	}

	@Get('get-pairs')
	@ApiOperation({ summary: 'Get pairs info' })
	@ApiOkResponse(joi2swaggerList(PairsDto))
	async getPairsInfo(): Promise<PairsDto[]> {
		return await this.paymentService.getPairsInfo();
	}

	@Post('estimate-amount')
	@ApiOperation({ summary: 'Estimate amount' })
	@ApiBody(joi2swagger(OrderDto))
	@ApiOkResponse(joi2swagger(EstimateAmountDto))
	async estimateAmount(@Body() orderDto: OrderDto): Promise<EstimateAmountDto> {
		return await this.paymentService.estimateAmount(orderDto.invoiceId, orderDto.clientCurrency);
	}

	@Get('get-currency-to/:invoiceId')
	@ApiOperation({ summary: 'Get the currency specified in the invoice' })
	@ApiParam({ name: 'invoiceId', schema: { type: 'string', format: 'uuid' } })
	@ApiOkResponse(joi2swagger(ResponseCurrencyToDto))
	async getCurrencyTo(@Param('invoiceId', ParseUUIDPipe) invoiceId: string): Promise<ResponseCurrencyToDto> {
		return await this.paymentService.getCurrencyTo(invoiceId);
	}

	@Post('webhook-handler')
	@Public()
	@ApiOperation({ summary: 'Payment invoice' })
	@ApiOkResponse({ status: 200 })
	async webhookHandler(@Body() order: OrderType): Promise<void> {
		await this.paymentService.webhookHandler(order);
	}
}
