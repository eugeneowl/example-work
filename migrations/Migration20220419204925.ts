import { Migration } from '@mikro-orm/migrations';

export class Migration20220419204925 extends Migration {

  async up(): Promise<void> {
    this.addSql('alter table "invoice" drop constraint "invoice_order_id_foreign";');

    this.addSql('alter table "user" drop constraint if exists "user_email_attachments_check";');
    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{ "invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" drop constraint if exists "user_email_notifications_check";');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{ }\';');
    this.addSql('alter table "user" drop constraint if exists "user_workflow_settings_check";');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{ "autoEmail": false, "autoArchive": false}\';');

    this.addSql('alter table "invoice" drop constraint "invoice_order_id_unique";');
    this.addSql('alter table "invoice" drop column "order_id";');

    this.addSql('alter table "order" add column "invoice_id" uuid not null, add column "payers_email" varchar(255) not null;');
    this.addSql('alter table "order" drop constraint if exists "order_order_check";');
    this.addSql('alter table "order" alter column "order" type jsonb using ("order"::jsonb);');
    this.addSql('alter table "order" alter column "order" set default \'{ }\';');
    this.addSql('alter table "order" add constraint "order_invoice_id_foreign" foreign key ("invoice_id") references "invoice" ("id") on update cascade;');
  }

  async down(): Promise<void> {
    this.addSql('alter table "order" drop constraint "order_invoice_id_foreign";');

    this.addSql('alter table "invoice" add column "order_id" uuid null default null;');
    this.addSql('alter table "invoice" add constraint "invoice_order_id_foreign" foreign key ("order_id") references "order" ("id") on update cascade on delete set null;');
    this.addSql('alter table "invoice" add constraint "invoice_order_id_unique" unique ("order_id");');

    this.addSql('alter table "order" drop constraint if exists "order_order_check";');
    this.addSql('alter table "order" alter column "order" type jsonb using ("order"::jsonb);');
    this.addSql('alter table "order" alter column "order" set default \'{}\';');
    this.addSql('alter table "order" drop column "invoice_id";');
    this.addSql('alter table "order" drop column "payers_email";');

    this.addSql('alter table "user" drop constraint if exists "user_email_attachments_check";');
    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{"invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" drop constraint if exists "user_email_notifications_check";');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{}\';');
    this.addSql('alter table "user" drop constraint if exists "user_workflow_settings_check";');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{"autoEmail": false, "autoArchive": false}\';');
  }

}
