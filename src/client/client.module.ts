import { Module } from '@nestjs/common';
import { ClientService } from './client.service';
import { ClientController } from './client.controller';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import ClientEntity from 'libs/database/entities/client.entity';
import InvoiceEntity from 'libs/database/entities/invoice.entity';

@Module({
	imports: [MikroOrmModule.forFeature([ClientEntity, InvoiceEntity])],
	providers: [ClientService],
	controllers: [ClientController],
})
export class ClientModule {}
