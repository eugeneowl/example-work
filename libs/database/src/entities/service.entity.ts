import { Entity, PrimaryKey, Property, ManyToOne, IdentifiedReference } from '@mikro-orm/core';
import InvoiceEntity from './invoice.entity';
import TagEntity from './tag.entity';

@Entity({ tableName: 'service' })
export default class ServiceEntity {
	@PrimaryKey({ type: 'uuid', defaultRaw: 'uuid_generate_v4()' })
	id!: string;

	@ManyToOne({ hidden: true, mapToPk: true })
	invoice!: IdentifiedReference<InvoiceEntity> | string;

	@Property()
	description!: string;

	@ManyToOne()
	tag!: IdentifiedReference<TagEntity>;

	@Property()
	rate!: number;

	@Property()
	qty!: number;

	@Property()
	amount!: number;
}
