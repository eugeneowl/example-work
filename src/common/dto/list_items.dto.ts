import Joi from 'joi';
import { getTypeSchema, JoiSchema } from 'nestjs-joi';

export class PageMetaDto {
	@JoiSchema(Joi.number().integer().required())
	page!: number;

	@JoiSchema(Joi.number().integer().required())
	itemCount!: number;

	@JoiSchema(Joi.number().integer().required())
	totalCount!: number;

	@JoiSchema(Joi.number().integer().required())
	pageCount!: number;

	@JoiSchema(Joi.bool().required())
	hasPreviousPage!: boolean;

	@JoiSchema(Joi.bool().required())
	hasNextPage!: boolean;
}

export class ListItemsDto<Type> {
	@JoiSchema(Joi.array().items(Joi.any()).required())
	data: Type[];

	@JoiSchema(getTypeSchema(PageMetaDto).required())
	meta: PageMetaDto;

	constructor() {
		this.data = [];
		this.meta = new PageMetaDto();
	}

	static init<Arg>(data: Arg[], page: number, totalCount: number, numberItemsPerPage: number): ListItemsDto<Arg> {
		const listItems = new this<Arg>();

		listItems.data = data;
		listItems.meta = new PageMetaDto();

		page = +page;

		listItems.meta.page = page;
		listItems.meta.itemCount = data.length;
		listItems.meta.totalCount = totalCount;
		listItems.meta.pageCount = Math.ceil(totalCount / numberItemsPerPage);
		listItems.meta.hasPreviousPage = page > 1;
		listItems.meta.hasNextPage = page < listItems.meta.pageCount;

		return listItems;
	}
}
