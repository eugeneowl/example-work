import { RedisModuleOptions } from '@liaoliaots/nestjs-redis';

export default (): { redis: RedisModuleOptions } => ({
	redis: {
		config: {
			host: process.env.REDIS_HOST,
			port: Number.parseInt(process.env.REDIS_PORT ?? '6379'),
		},
	},
});
