import Joi from 'joi';
import { JoiSchema } from 'nestjs-joi';

export class ResponseCurrencyToDto {
	@JoiSchema(Joi.string().required())
	to!: string;
}
