import Joi, { ObjectSchema } from 'joi';
import { DEFAULT, getTypeSchema, JoiValidationGroup } from 'nestjs-joi';
import j2s, { SwaggerSchema } from 'joi-to-swagger';
import { ListItemsDto } from '../dto/list_items.dto';

export default function joi2swagger<Type, ListItemType>(
	dto: Type,
	group: JoiValidationGroup = DEFAULT,
	listItemType?: ListItemType
): { schema: SwaggerSchema } {
	let joiObject = getTypeSchema(dto as any, { group });

	if (listItemType !== undefined) {
		joiObject = (joiObject as ObjectSchema<ListItemsDto<ListItemType>>).keys({
			data: Joi.array()
				.items(getTypeSchema(listItemType as any, { group }).required())
				.required(),
		});
	}

	const schema = {
		schema: j2s(joiObject).swagger,
	};

	return schema;
}

export function joi2swaggerList<Type>(dto: Type, group: JoiValidationGroup = DEFAULT): { schema: SwaggerSchema } {
	const joiObject = Joi.array()
		.items(getTypeSchema(dto as any, { group }).required())
		.required();

	const schema = {
		schema: j2s(joiObject).swagger,
	};

	return schema;
}
