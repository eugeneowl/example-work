import { Entity, PrimaryKey, Property, Enum, ManyToOne, OptionalProps } from '@mikro-orm/core';
import NotificationEvent from '../enums/notification_event.enum';
import UserEntity from './user.entity';

export { NotificationEvent };

@Entity({ tableName: 'notification' })
export class NotificationEntity {
	[OptionalProps]?: 'status' | 'createdAt';

	@PrimaryKey({ type: 'uuid', defaultRaw: 'uuid_generate_v4()' })
	id!: string;

	@ManyToOne(() => UserEntity, { mapToPk: true, hidden: true })
	user!: string;

	@Property()
	message!: string;

	@Enum({ items: ['unread', 'read'] })
	status: 'unread' | 'read' = 'unread';

	@Enum(() => NotificationEvent)
	key!: NotificationEvent;

	@Property()
	createdAt = new Date();
}
