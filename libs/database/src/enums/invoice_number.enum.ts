enum InvoiceNumber {
	Prefix = 'prefix',
	Counter = 'counter',
}

export default InvoiceNumber;
