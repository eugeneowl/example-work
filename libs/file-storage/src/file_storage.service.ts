import { GetObjectCommand, S3 } from '@aws-sdk/client-s3';
import { Upload } from '@aws-sdk/lib-storage';
import { getSignedUrl } from '@aws-sdk/s3-request-presigner';
import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Readable } from 'stream';

@Injectable()
export class FileStorageService {
	readonly #bucketInvoice: string;
	readonly #bucketPicture: string;

	readonly bucketPictureUrl: string;

	constructor(@Inject('AWS_S3_INSTANCE') private readonly s3: S3, configService: ConfigService) {
		this.#bucketInvoice = configService.get('AWS_BUCKET_INVOICE') ?? '';
		this.#bucketPicture = configService.get('AWS_BUCKET_PICTURE') ?? '';

		this.bucketPictureUrl = `https://${this.#bucketPicture}.s3.eu-central-1.amazonaws.com/`;
	}

	public async saveInvoice(invoiceId: string, pdfStream: Readable): Promise<void> {
		const upload = new Upload({
			client: this.s3,
			params: {
				Bucket: this.#bucketInvoice,
				Key: invoiceId + '.pdf',
				Body: pdfStream,
			},
		});

		await upload.done();
	}

	public async getInvoiceUrl(invoiceId: string): Promise<string> {
		const command = new GetObjectCommand({
			Bucket: this.#bucketInvoice,
			Key: invoiceId + '.pdf',
		});

		return await getSignedUrl(this.s3, command, { expiresIn: 3600 });
	}

	public async getInvoiceStream(invoiceId: string): Promise<Readable | undefined> {
		const response = await this.s3.getObject({
			Bucket: this.#bucketInvoice,
			Key: invoiceId + '.pdf',
		});

		return response.Body as any;
	}

	public async saveImage(uuid: string, logo: Buffer, ext: string, type: 'logo' | 'avatar' | 'client' | 'invoice'): Promise<string> {
		const prefix = type.at(0);
		const fileName = `${prefix}-${uuid}${ext}`;
		const upload = new Upload({
			client: this.s3,
			params: {
				Bucket: this.#bucketPicture,
				Key: fileName,
				Body: logo,
			},
		});

		await upload.done();

		return this.bucketPictureUrl + fileName;
	}
}
