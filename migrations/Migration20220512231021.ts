import { Migration } from '@mikro-orm/migrations';

export class Migration20220512231021 extends Migration {

  async up(): Promise<void> {
    this.addSql('alter table "invoice" drop column "date";');
  }

  async down(): Promise<void> {
    this.addSql('alter table "invoice" add column "date" date not null default null;');
  }

}
