import Joi from 'joi';
import OrderStatus from 'libs/database/enums/order_status.enum';
import { JoiSchema } from 'nestjs-joi';

export class OrderAliceBobDto {
	@JoiSchema(Joi.number().required())
	extraFromFee!: number;

	@JoiSchema(Joi.number().required())
	extraToFee!: number;

	@JoiSchema(Joi.number().required())
	from!: string;

	@JoiSchema(Joi.number().required())
	fromAmount!: number;

	@JoiSchema(Joi.number().required())
	fromAmountReceived!: number;

	@JoiSchema(Joi.number().required())
	fromFee!: number;

	@JoiSchema(Joi.number().required())
	fromRate!: number;

	@JoiSchema(Joi.string().required())
	fromTxHash!: string;

	@JoiSchema(Joi.string().required())
	id!: string;

	@JoiSchema(Joi.string().required())
	partnerOrderId!: string;

	@JoiSchema(Joi.string().required())
	payCryproMemo!: string;

	@JoiSchema(Joi.string().required())
	payCryptoAddress!: string;

	@JoiSchema(Joi.string().required())
	payUrl!: string;

	@JoiSchema(Joi.string().valid('FLOATING', 'FIXED').required())
	rateType!: 'FLOATING' | 'FIXED';

	@JoiSchema(Joi.string().required())
	redirectUrl!: string;

	@JoiSchema(
		Joi.string()
			.valid(...Object.values(OrderStatus))
			.required()
	)
	status!: OrderStatus;

	@JoiSchema(Joi.string().required())
	to!: string;

	@JoiSchema(Joi.number().required())
	toAmount!: number;

	@JoiSchema(Joi.number().required())
	toFee!: number;

	@JoiSchema(Joi.string().required())
	toMemo!: string;

	@JoiSchema(Joi.number().required())
	toRate!: number;

	@JoiSchema(Joi.string().required())
	toTxHash!: string;

	@JoiSchema(Joi.string().required())
	userId!: string;
}
