import { Entity, PrimaryKey, Property, Unique, ManyToOne, OptionalProps } from '@mikro-orm/core';
import { SoftDeletable } from 'mikro-orm-soft-delete';
import UserEntity from './user.entity';

@Entity({ tableName: 'tag' })
@Unique({ properties: ['name', 'user'] })
@SoftDeletable(() => TagEntity, 'deletedAt', () => new Date())
export default class TagEntity {
	[OptionalProps]?: 'isDelete';

	@PrimaryKey({ type: 'uuid', defaultRaw: 'uuid_generate_v4()' })
	id!: string;

	@Property({ length: 50 })
	name!: string;

	@ManyToOne(() => UserEntity, { mapToPk: true, hidden: true })
	user?: string;

	@Property({ nullable: true, hidden: true })
	deletedAt?: Date | null;
}
