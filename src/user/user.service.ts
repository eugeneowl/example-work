import { InjectRepository } from '@mikro-orm/nestjs';
import { EntityRepository } from '@mikro-orm/postgresql';
import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { UserDto } from './dto/user.dto';
import UserEntity from 'libs/database/entities/user.entity';
import UserEmailEntity from 'libs/database/entities/user_email.entity';
import { UpdateUserEmailsResponseDto } from './dto/update_user_emails_response.dto';
import { NotificationService } from 'src/notification/notification.service';
import { FileStorageService } from 'libs/file-storage';
import path from 'path';
import { ImageResponseDto } from '../common/dto/image_response.dto';
import { UserEmailDto } from './dto/user_email.dto';
import UserEmailStatus from 'libs/database/enums/user_email_status.enum';

@Injectable()
export class UserService {
	constructor(
		@InjectRepository(UserEntity)
		private readonly userRepository: EntityRepository<UserEntity>,
		@InjectRepository(UserEmailEntity)
		private readonly userEmailRepository: EntityRepository<UserEmailEntity>,
		private readonly notificationService: NotificationService,
		private readonly fileStorageService: FileStorageService
	) {}

	public async updateUser(id: string, newUserData: UserDto): Promise<void> {
		const user = await this.userRepository.findOne(id);

		if (user == null) {
			throw new NotFoundException('User not found');
		}

		this.userRepository.assign(user, newUserData);

		await this.userRepository.persistAndFlush(user);
	}

	public async getUser(id: string): Promise<UserDto> {
		const user = await this.userRepository.findOne(id, { populate: ['emails', 'wallets'] });

		if (user == null) {
			throw new NotFoundException('User not found');
		}

		return user;
	}

	public async updateEmailList(id: string, emails: UserEmailDto[]): Promise<UpdateUserEmailsResponseDto> {
		const response = new UpdateUserEmailsResponseDto();
		const user = await this.userRepository.findOne(id, { populate: ['emails'] });

		if (user == null) {
			throw new NotFoundException('User not found');
		}

		const result = emails.reduce(
			(result, userEmail) => {
				result[Number(userEmail.id !== undefined)].push(userEmail.email);

				if (userEmail.isDefault) {
					result[2].push(userEmail.email);
				}

				return result;
			},
			[[], [], []] as [string[], string[], string[]]
		);

		let [newEmails, existEmails] = result;
		const [, , defaultEmails] = result;

		if (defaultEmails.length == 0) {
			throw new BadRequestException('Default email not specified');
		}

		if (defaultEmails.length > 1) {
			throw new BadRequestException('More than one default email specified');
		}

		newEmails = [...new Set(newEmails)];
		existEmails = [...new Set(existEmails)];

		const duplicateEmails = await this.userEmailRepository.find({ email: { $in: newEmails } });

		for (const duplicateEmail of duplicateEmails) {
			response.duplicate.push(duplicateEmail.email);
		}

		if (duplicateEmails.length > 0) {
			return response;
		}

		if (existEmails.length > 0) {
			const removeIds: UserEmailEntity[] = user.emails
				.getItems()
				.filter((emailEntity) => existEmails.includes(emailEntity.email) == false);

			if (user.emails.count() == removeIds.length && newEmails.length == 0) {
				throw new BadRequestException("Can't delete all emails");
			}

			user.emails.remove(...removeIds);
		} else {
			user.emails.removeAll();
		}

		if (newEmails.length > 0) {
			const insertedValues = newEmails.map(
				(email): UserEmailEntity => this.userEmailRepository.create({ user: id, email })
			);

			user.emails.add(...insertedValues);
		}

		for (const userEmail of user.emails.getItems()) {
			userEmail.isDefault = userEmail.email == defaultEmails[0];
		}

		await this.userRepository.persistAndFlush(user);

		response.newEmailList = user.emails.getItems();

		return response;
	}

	public async verifyEmail(id: string, emailId: string, frontendUrl?: string): Promise<void> {
		const email = await this.userEmailRepository.findOne({ id: emailId, user: id });

		if (email == null) {
			throw new NotFoundException('Email not found');
		}

		switch (email.status) {
			case UserEmailStatus.NotVerified:
				email.status = UserEmailStatus.PendingVerification;
				break;
			case UserEmailStatus.Verified:
				throw new BadRequestException('Email already verified');
				break;
		}

		await this.userEmailRepository.persistAndFlush(email);
		await this.notificationService.notifyEmailVerification(id, email.id, email.email, frontendUrl);
	}

	public async confirmEmail(id: string, token: string): Promise<void> {
		const email = await this.userEmailRepository.findOne({ id: token, user: id });

		if (email == null) {
			throw new NotFoundException('Email not found');
		}

		if (email.status == UserEmailStatus.Verified) {
			throw new BadRequestException('Email already verified');
		}

		email.status = UserEmailStatus.Verified;

		await this.userEmailRepository.persistAndFlush(email);
	}

	public async saveImage(id: string, file: Express.Multer.File, type: 'logo' | 'avatar'): Promise<ImageResponseDto> {
		const user = await this.userRepository.findOne(id);

		if (user == null) {
			throw new NotFoundException('User not found');
		}

		const url = await this.fileStorageService.saveImage(id, file.buffer, path.extname(file.originalname), type);

		if (type == 'avatar') {
			user.avatarUrl = url;
		} else {
			user.logoUrl = url;
		}

		await this.userRepository.persistAndFlush(user);

		return { image: url };
	}
}
