import { Migration } from '@mikro-orm/migrations';

export class Migration20220318190339 extends Migration {

  async up(): Promise<void> {
    this.addSql('create table "user_email" ("id" uuid not null default uuid_generate_v4(), "email" varchar(255) not null, "is_default" boolean not null, "is_verified" boolean not null, "user_id" uuid not null);');
    this.addSql('alter table "user_email" add constraint "user_email_email_unique" unique ("email");');
    this.addSql('alter table "user_email" add constraint "user_email_pkey" primary key ("id");');

    this.addSql('alter table "user_email" add constraint "user_email_user_id_foreign" foreign key ("user_id") references "user" ("id") on update cascade;');

    this.addSql('alter table "user" drop constraint if exists "user_email_attachments_check";');
    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{ "invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" drop constraint if exists "user_email_notifications_check";');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{ }\';');
    this.addSql('alter table "user" drop constraint if exists "user_workflow_settings_check";');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{ "autoEmail": false, "autoArchive": false}\';');
    this.addSql('alter table "user" drop constraint "user_email_unique";');
    this.addSql('alter table "user" drop column "email";');
  }

  async down(): Promise<void> {
    this.addSql('drop table if exists "user_email" cascade;');

    this.addSql('alter table "user" add column "email" varchar not null default null;');
    this.addSql('alter table "user" drop constraint if exists "user_email_attachments_check";');
    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{"invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" drop constraint if exists "user_email_notifications_check";');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{}\';');
    this.addSql('alter table "user" drop constraint if exists "user_workflow_settings_check";');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{"autoEmail": false, "autoArchive": false}\';');
    this.addSql('alter table "user" add constraint "user_email_unique" unique ("email");');
  }

}
