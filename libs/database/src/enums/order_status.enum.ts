enum OrderStatus {
	Waiting = 'WAITING',
	Exchanging = 'EXCHANGING',
	Sending = 'SENDING',
	Completed = 'COMPLETED',
	NotEntireWithdraw = 'NOT_ENTIRE_WITHDRAW',
	Expired = 'EXPIRED',
	Canceled = 'CANCELED',
	Failed = 'FAILED',
	Holded = 'HOLDED',
	Prepared = 'PREPARED',
}

export default OrderStatus;
