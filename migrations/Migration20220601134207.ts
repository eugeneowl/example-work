import { Migration } from '@mikro-orm/migrations';

export class Migration20220601134207 extends Migration {

  async up(): Promise<void> {
    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{ "invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{ }\';');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{ "autoEmail": false, "autoArchive": false}\';');

    this.addSql('alter table "client" drop column "avatar_url";');

    this.addSql('alter table "order" alter column "order" type jsonb using ("order"::jsonb);');
    this.addSql('alter table "order" alter column "order" set default \'{ }\';');
  }

  async down(): Promise<void> {
    this.addSql('alter table "client" add column "avatar_url" varchar null default null;');

    this.addSql('alter table "order" alter column "order" type jsonb using ("order"::jsonb);');
    this.addSql('alter table "order" alter column "order" set default \'{}\';');

    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{"invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{}\';');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{"autoEmail": false, "autoArchive": false}\';');
  }

}
