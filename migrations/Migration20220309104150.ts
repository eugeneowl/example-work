import { Migration } from '@mikro-orm/migrations';

export class Migration20220309104150 extends Migration {

  async up(): Promise<void> {
    this.addSql('alter table "user" drop constraint if exists "user_email_attachments_check";');
    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{ "invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" drop constraint if exists "user_email_notifications_check";');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{ }\';');
    this.addSql('alter table "user" drop constraint if exists "user_workflow_settings_check";');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{ "autoEmail": false, "autoArchive": false}\';');

    this.addSql('alter table "client" add column "user_id" uuid not null;');
    this.addSql('alter table "client" add constraint "client_user_id_foreign" foreign key ("user_id") references "user" ("id") on update cascade;');

    this.addSql('alter table "invoice" drop constraint if exists "invoice_status_check";');
  }

  async down(): Promise<void> {
    this.addSql('alter table "client" drop constraint "client_user_id_foreign";');

    this.addSql('alter table "client" drop column "user_id";');

    this.addSql('alter table "invoice" drop constraint if exists "invoice_status_check";');
    this.addSql('alter table "invoice" alter column "status" type text using ("status"::text);');
    this.addSql('alter table "invoice" add constraint "invoice_status_check" check ("status" in (\'invoice_saved\'));');

    this.addSql('alter table "user" drop constraint if exists "user_email_attachments_check";');
    this.addSql('alter table "user" alter column "email_attachments" type jsonb using ("email_attachments"::jsonb);');
    this.addSql('alter table "user" alter column "email_attachments" set default \'{"invoice": false, "receipt": false}\';');
    this.addSql('alter table "user" drop constraint if exists "user_email_notifications_check";');
    this.addSql('alter table "user" alter column "email_notifications" type jsonb using ("email_notifications"::jsonb);');
    this.addSql('alter table "user" alter column "email_notifications" set default \'{}\';');
    this.addSql('alter table "user" drop constraint if exists "user_workflow_settings_check";');
    this.addSql('alter table "user" alter column "workflow_settings" type jsonb using ("workflow_settings"::jsonb);');
    this.addSql('alter table "user" alter column "workflow_settings" set default \'{"autoEmail": false, "autoArchive": false}\';');
  }

}
