import { InjectRepository } from '@mikro-orm/nestjs';
import { EntityRepository } from '@mikro-orm/postgresql';
import { QueryFlag, UniqueConstraintViolationException, QueryOrderMap } from '@mikro-orm/core';
import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import ClientEntity from 'libs/database/entities/client.entity';
import { ClientDto } from './dto/client.dto';
import { ListItemsDto } from 'src/common/dto/list_items.dto';
import ClientListSort from './types/client_list_sort.enum';
import OrderBy from 'src/common/types/order_by.enum';
import InvoiceStatus from 'libs/database/enums/invoice_status.enum';
import InvoiceEntity from 'libs/database/entities/invoice.entity';

@Injectable()
export class ClientService {
	readonly #numberItemsPerPage = 50;

	constructor(
		@InjectRepository(ClientEntity)
		private readonly clientRepository: EntityRepository<ClientEntity>,
		@InjectRepository(InvoiceEntity)
		private readonly invoiceRepository: EntityRepository<InvoiceEntity>
	) {}

	public async createClient(userId: string, client: ClientDto): Promise<ClientDto> {
		const newClient = this.clientRepository.create({ user: userId, ...client });

		try {
			await this.clientRepository.persistAndFlush(newClient);
		} catch (error) {
			if (error instanceof UniqueConstraintViolationException) {
				throw new BadRequestException('A client with this email already exists');
			}

			throw error;
		}

		await this.clientRepository.persistAndFlush(newClient);

		return newClient;
	}

	public async updateClient(userId: string, id: string, newClientData: ClientDto): Promise<ClientDto> {
		const client = await this.clientRepository.findOne({ id, user: userId });

		if (client == null) {
			throw new NotFoundException('Client not found');
		}

		this.clientRepository.assign(client, newClientData);

		await this.clientRepository.persistAndFlush(client);

		return client;
	}

	public async listClients(
		userId: string,
		page = 1,
		sortBy?: ClientListSort,
		orderBy: OrderBy = OrderBy.ASC,
		status?: InvoiceStatus
	): Promise<ListItemsDto<ClientDto>> {
		if (page < 1) {
			throw new BadRequestException('Page cannot be less than 1');
		}

		let where = 'client_id = client.id';
		let whereParams: any[] = [];

		if (status != undefined) {
			where = 'client_id = client.id AND status = ?';
			whereParams = [status];
		}

		let orderByObject: QueryOrderMap<ClientEntity> = {};

		switch (sortBy) {
			case ClientListSort.ClientName:
				orderByObject = {
					firstName: orderBy,
					lastName: orderBy,
				};
				break;
			case ClientListSort.Total:
				orderByObject = {
					totalAmountInvoices: orderBy,
				};
				break;
		}

		const selectNumberInvoices = this.invoiceRepository
			.qb()
			.select('COUNT(id)::INTEGER')
			.where(where, whereParams)
			.as('numberInvoices')
			.select();

		const selectTotalAmountInvoices = this.invoiceRepository
			.qb()
			.select('COALESCE(SUM(total), 0)::INTEGER')
			.where(where, whereParams)
			.as('totalAmountInvoices')
			.select();

		const clients = await this.clientRepository
			.qb('client')
			.setFlag(QueryFlag.PAGINATE)
			.select([
				'id',
				'email',
				'firstName',
				'lastName',
				'companyName',
				'title',
				'addressOne',
				'addressTwo',
				'city',
				'country',
				'zip',
				selectNumberInvoices,
				selectTotalAmountInvoices,
			])
			.offset((page - 1) * this.#numberItemsPerPage)
			.limit(this.#numberItemsPerPage)
			.orderBy(orderByObject)
			.execute();

		const count = await this.clientRepository.count({ user: userId });
		const response = ListItemsDto.init(clients, page, count, this.#numberItemsPerPage);

		return response;
	}

	public async getClient(userId: string, id: string): Promise<ClientDto> {
		const client = await this.clientRepository.findOne({ id, user: userId });

		if (client == null) {
			throw new NotFoundException('Client not found');
		}

		return client;
	}
}
