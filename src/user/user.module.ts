import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import UserEntity from 'libs/database/entities/user.entity';
import UserEmailEntity from 'libs/database/entities/user_email.entity';
import { NotificationModule } from 'src/notification/notification.module';
import { FileStorageModule } from 'libs/file-storage';

@Module({
	imports: [MikroOrmModule.forFeature([UserEntity, UserEmailEntity]), NotificationModule, FileStorageModule],
	providers: [UserService],
	controllers: [UserController],
})
export class UserModule {}
