import { Entity, PrimaryKey, Property, Unique, ManyToOne, Formula } from '@mikro-orm/core';
import UserEntity from './user.entity';

@Entity({ tableName: 'client' })
@Unique({ properties: ['user', 'email'] })
export default class ClientEntity {
	@PrimaryKey({ type: 'uuid', defaultRaw: 'uuid_generate_v4()' })
	id!: string;

	@Property()
	email!: string;

	@Property({ length: 50 })
	firstName!: string;

	@Property({ length: 50 })
	lastName!: string;

	@Property()
	companyName!: string;

	@Property()
	title!: string;

	@Property()
	addressOne!: string;

	@Property()
	addressTwo!: string;

	@Property()
	city!: string;

	@Property({ length: 100 })
	country!: string;

	@Property({ length: 18 })
	zip!: string;

	@ManyToOne(() => UserEntity, { mapToPk: true, lazy: true })
	user!: string;

	@Formula((alias) => `(SELECT COUNT(id)::INTEGER FROM invoice WHERE invoice.client_id = ${alias}.id)`, { lazy: true })
	numberInvoices?: number;

	@Formula((alias) => `(SELECT COALESCE(SUM(total), 0)::INTEGER FROM invoice WHERE invoice.client_id = ${alias}.id)`, { lazy: true })
	totalAmountInvoices?: number;
}
