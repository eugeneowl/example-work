import Joi from 'joi';
import UserEmailStatus from 'libs/database/enums/user_email_status.enum';
import { CREATE, JoiSchema } from 'nestjs-joi';

export class UserEmailDto {
	@JoiSchema(Joi.string().uuid().forbidden())
	@JoiSchema([CREATE], Joi.string().uuid().optional())
	@JoiSchema(['LIST'], Joi.string().uuid().required())
	id?: string;

	@JoiSchema(Joi.string().email().max(255).required())
	email!: string;

	@JoiSchema(Joi.boolean().required())
	isDefault!: boolean;

	@JoiSchema(
		Joi.string()
			.valid(...Object.values(UserEmailStatus))
			.required()
	)
	@JoiSchema([CREATE], Joi.boolean().forbidden())
	status!: UserEmailStatus;
}
