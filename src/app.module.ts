import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AppService } from './app.service';
import { EnvironmentVariables } from './config/env.validation';
import redisConfiguration from './config/redis.configuration';
import { InvoiceModule } from './invoice/invoice.module';
import { ClientModule } from './client/client.module';
import { DatabaseModule } from 'libs/database';
import { getTypeSchema, JoiPipeModule } from 'nestjs-joi';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { TagModule } from './tag/tag.module';
import { RedisModule, RedisModuleOptions } from '@liaoliaots/nestjs-redis';
import { NotificationModule } from './notification/notification.module';
import { PictureModule } from './picture/picture.module';
import { PaymentModule } from './payment/payment.module';
import { LoggerModule } from 'nestjs-pino';

@Module({
	imports: [
		LoggerModule.forRoot(),
		JoiPipeModule.forRoot({
			pipeOpts: {
				defaultValidationOptions: {
					allowUnknown: false,
				},
			},
		}),
		ConfigModule.forRoot({
			validationSchema: getTypeSchema(EnvironmentVariables),
			isGlobal: true,
			load: [redisConfiguration],
		}),
		RedisModule.forRootAsync({
			inject: [ConfigService],
			useFactory: async (configService: ConfigService): Promise<RedisModuleOptions> => {
				return configService.get('redis') ?? {};
			},
		}),
		DatabaseModule,
		TagModule,
		ClientModule,
		UserModule,
		InvoiceModule,
		AuthModule,
		NotificationModule,
		PictureModule,
		PaymentModule,
	],
	controllers: [],
	providers: [AppService],
})
export class AppModule {}
