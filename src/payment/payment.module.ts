import { Module } from '@nestjs/common';
import { PaymentService } from './payment.service';
import { PaymentController } from './payment.controller';
import { ConfigModule } from '@nestjs/config';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { OrderEntity } from 'libs/database/entities/order.entity';
import InvoiceEntity from 'libs/database/entities/invoice.entity';
import WalletEntity from 'libs/database/entities/wallet.entity';
import { NotificationModule } from 'src/notification/notification.module';
import ClientEntity from 'libs/database/entities/client.entity';

@Module({
	imports: [ConfigModule, MikroOrmModule.forFeature([InvoiceEntity, OrderEntity, WalletEntity, ClientEntity]), NotificationModule],
	providers: [PaymentService],
	controllers: [PaymentController],
})
export class PaymentModule {}
