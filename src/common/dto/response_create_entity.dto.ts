import Joi from 'joi';
import { JoiSchema } from 'nestjs-joi';

export class ResponseCreateEntityDto {
	@JoiSchema(Joi.string().uuid().required())
	id!: string;
}
